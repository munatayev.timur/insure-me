# Insure Me

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

[Official website](https://insureme.snappik.app)

A reliable way to let your family or friends know that you're not in danger.  Keep yourself safe in dangerous or stressful situations. The app will notify your family or friends that something has happened to you.

## Description

InsureMe is designed specialy to avoid worries from the user and their loved ones in any dangerous situations. Application will check according to the settings if the owner of the phone has access to his device and if he is in danger.  In case the user doesn't turn off the notification in a short period of time(by asking to confirm that the user is okay), InsureMe app immediately inform the specified addressee (for example, relatives) that the user has no access to the phone or something has happened to him/her.
You can use it when meeting suspicious or unfamiliar people, when taking strong medication (we recommend a short check-up period), when walking through unfavourable areas and many other situations that make you worry about your safety or the safety of a dear person.
InsureMe cultivates responsibility to the family and friends who care about you.  Don't make your family worry, and you can always be sure that they will come to your rescue.

### Bulet features

- simple
- light weight
- no location data
- encrypted user data
- working without internet
- all data stored on the phone, no servers
- greate UI and animations
- friends system
- fall detector
- multiple accounts support
- second protection layer supported by PIN

## How use
1. **Configuring the application**. Set the settings that works best for you, such as "Message delivery method", "Lock type" and "Alarm time".
2. **Starting a timer**. When you start the timer, the app will start a countdown to the moment of the signal. During this time you can disable the timer by unlocking the app.
3. **User status check**. When the timer expires, the app will give the user 1 minute to confirm that he/she is okay by unlocking the app.
4. **Message sending**. If the user does not take any action within the specified minute or is unable to unlock the application, InsureMe considers the situation to be life-threatening and sends a message to the addressee specified earlier.
5. **Fall detector**. By enabling fall detector service, application after fall will ask the user to confirm that he/she is in a good health or message sending service will be fired.


### What used

- MVVM architecture
- Motion layout
- Kotlin
- Kotlin DSL
- Kotlin coroutines
- Data binding
- Room database
- LiveData
- Hilt dependancy injection
- HMS Core (Billing, Push Kit, Nearby, Account Kit, Analytics Kit, Map Kit)
- Google Services (Billing, OAuth 2.0, Nearby, Map)
- Firebase (Push, Analytics)
- Foreground service
- Alarm manager with Wake lock
- Custom notifications
- Retrofit REST API

### Personal data

[Privacy Policy](https://insureme.snappik.app/privacy_policy.html "Privacy Policy")
[Terms &amp; Conditions](https://insureme.snappik.app/terms_and_conditions.html "Terms &amp; Conditions")

### Keywords

insurance, notification, timer, security, alarm, count, lockscreen, family, relatives, down, insureme

## Download

<a href='https://play.google.com/store/apps/details?id=app.snappik.insureme&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' width="300" height="120" src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>

<a href='https://appgallery.cloud.huawei.com/ag/n/app/C102770827?channelId=gitlab_repository&referrer=https%3A%2F%2Fappgallery.huawei.com%2F%23%2Fapp%2FC102770827&id=3c284c7a90914cd2b1128b1080e9b773&s=C9F6B3F99E68126D57578D5BB3B79D001F039E22F6A86696FCE3EB6CDB41ABAA&detailType=0&v='><img alt='Get it on App Gallery' width="300" height="120" src='https://huaweimobileservices.com/wp-content/uploads/2020/05/Explore-it-on-AppGallery.png'/></a>


## Authors

* **Munatayev Timur**

## License

Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0
