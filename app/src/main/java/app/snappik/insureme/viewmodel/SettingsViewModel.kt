package app.snappik.insureme.viewmodel

import androidx.annotation.UiThread
import androidx.lifecycle.LiveData
import app.snappik.insureme.database.repository.SettingsRepository
import app.snappik.insureme.model.AlarmType
import app.snappik.insureme.model.Strategy
import app.snappik.insureme.model.UnlockType
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.model.entity.settings
import app.snappik.insureme.utils.Utilities.getMD5
import kotlinx.coroutines.launch

class SettingsViewModel : AbstractViewModel() {

    var settings: LiveData<Settings>? = null

    var settingsRepository: SettingsRepository? = null
        set(value) {
            field = value
            settings = value?.getSettings()
        }

    @UiThread
    fun updateDelay(mDelay: Long) = dbScope.launch {
        settings?.value?.let { settingsRepository?.updateSettings(it.apply { delay = mDelay }) }
    }

    @UiThread
    fun updatePin(mPin: String) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                pin = mPin.getMD5()
                pinLength = mPin.length
                reversedPin = mPin.reversed().getMD5()
            })
        }
    }

    @UiThread
    fun updatePanicPin(isPanic: Boolean) = dbScope.launch {
        settings?.value?.let { settingsRepository?.updateSettings(it.apply { panicPIN = isPanic }) }
    }

    @UiThread
    fun updateAttempts(mAttempts: Int) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                amountOfAttempts = mAttempts
            })
        }
    }

    @UiThread
    fun updateMessage(mMessage: String) = dbScope.launch {
        settings?.value?.let { settingsRepository?.updateSettings(it.apply { message = mMessage }) }
    }

    @UiThread
    fun updateDestinationNumber(number: String) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                destinationNumber = number
            })
        }
    }

    @UiThread
    fun updateUnlockType(mUnlockType: UnlockType) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                unlockType = mUnlockType
            })
        }
    }

    @UiThread
    fun updateStrategies(mStrategies: HashSet<Strategy>) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                strategies = mStrategies
            })
        }
    }

    @UiThread
    fun updateSendLocation(mSendLocation: Boolean) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                sendLocation = mSendLocation
            })
        }
    }

    @UiThread
    fun updateTime(mTime: Long) = dbScope.launch {
        settings?.value?.let { settingsRepository?.updateSettings(it.apply { time = mTime }) }
    }

    @UiThread
    fun updateAlarmType(mAlarmType: AlarmType) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                alarmType = mAlarmType
            })
        }
    }

    @UiThread
    fun updateSubscriptionStatus(mIsPremium: Boolean) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                isPremium = mIsPremium
            })
        }
    }

    @UiThread
    fun updateFallDetector(fallDetectorEnabled: Boolean) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                fallDetector = fallDetectorEnabled
            })
        }
    }

    @UiThread
    fun nullifySettings() = dbScope.launch {
        settings?.value?.let {
            val newSettings = settings { id = it.id }
            settingsRepository?.updateSettings(newSettings)
        }
    }

    @UiThread
    fun updateEmailSurname(mEmail: String, fullName: String) = dbScope.launch {
        settings?.value?.let {
            settingsRepository?.updateSettings(it.apply {
                email = mEmail
                nameSurname = fullName
            })
        }
    }
}