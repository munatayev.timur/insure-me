package app.snappik.insureme.viewmodel

import android.content.Context
import androidx.annotation.UiThread
import androidx.lifecycle.LiveData
import app.snappik.insureme.R
import app.snappik.insureme.database.repository.UserRepository
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.model.entity.user
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserViewModel : AbstractViewModel() {

    var user: LiveData<User>? = null

    var userRepository: UserRepository? = null
        set(value) {
            field = value
            user = value?.getUser()
        }

    @UiThread
    fun signInAsGuest(context: Context) = dbScope.launch {
        val newUser = user {
            displayName = context.getString(R.string.login_guest)
        }
        userRepository?.signInUser(newUser)
    }

    @UiThread
    fun signIn(displayName: String, type: User.Type) = dbScope.launch {
        val newUser = user {
            this.displayName = displayName
            this.type = type
        }
        userRepository?.signInUser(newUser)
    }

    @UiThread
    fun applyTermsAndConditions(user: User) = dbScope.launch { userRepository?.applyTermsAndConditions(user.id) }

    @UiThread
    fun deleteUser(onSuccess:() -> Unit) = mainScope.launch {
        withContext(Dispatchers.IO) {
            user?.value?.let {
                userRepository?.deleteUser(it)
            }
        }
        onSuccess.invoke()
    }
}