package app.snappik.insureme.viewmodel

import androidx.annotation.UiThread
import androidx.lifecycle.LiveData
import app.snappik.insureme.database.repository.FriendRepository
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.model.entity.friend
import kotlinx.coroutines.launch

class FriendViewModel: AbstractViewModel() {

    var friends: LiveData<List<Friend>>? = null

    var friendRepository: FriendRepository? = null
        set(value) {
            field = value
            friends = value?.getAll()
        }

    @UiThread
    fun deleteAll() = dbScope.launch { friendRepository?.deleteAll() }

    fun deleteByToken(token: String) = dbScope.launch { friendRepository?.deleteFriendByToken(token) }

    @UiThread
    fun delete(friend: Friend) = dbScope.launch { friendRepository?.deleteFriend(friend) }

    @UiThread
    fun updateFriend(friend: Friend) = dbScope.launch { friendRepository?.update(friend) }

    @UiThread
    fun insert(name: String, pushToken: String) = dbScope.launch {
        val newFriend = friend {
            displayName = name
            token = pushToken
        }
        friendRepository?.insert(newFriend)
    }

    @UiThread fun addFriendToSend(friend: Friend) = dbScope.launch {
        friends?.value?.forEach {
            friendRepository?.update(it.apply { selected = it.id == friend.id })
        }
    }
}