package app.snappik.insureme.viewmodel

import android.content.Context
import app.snappik.insureme.R
import app.snappik.insureme.model.request.GooglePushRequest
import app.snappik.insureme.model.request.HuaweiPushRequest
import app.snappik.insureme.retrofit.RetrofitRepository
import com.google.auth.oauth2.GoogleCredentials
import com.huawei.agconnect.config.AGConnectServicesConfig
import kotlinx.coroutines.launch

class RetrofitViewModel : AbstractViewModel() {

    var retrofitRepository: RetrofitRepository? = null

    suspend fun sendHuaweiPush(context: Context, accessToken: String, huaweiPushRequest: HuaweiPushRequest){
        val appId = AGConnectServicesConfig.fromContext(context).getString(APP_ID_REQUEST)
        retrofitRepository?.sendPush("v1/${appId}", accessToken, huaweiPushRequest)
    }

    fun getAccessTokenGoogle(context: Context, onFinish: (response: String?) -> Unit) = dbScope.launch {
        dbScope.runCatching {
            val file = context.assets.open(SERVICE_ACCOUNT_FILE)
            val credentials =
                GoogleCredentials.fromStream(file).createScoped(GOOGLE_FCM_MESSAGE_URL)
            credentials.refreshIfExpired()
            credentials.accessToken
        }.onSuccess { accessToken ->
            accessToken?.tokenValue?.let {
                onFinish.invoke(it)
            }
        }.onFailure {
            onFinish.invoke(null)
        }
    }

    suspend fun sendGooglePush(context: Context, accessToken: String, push: GooglePushRequest){
        retrofitRepository?.sendGooglePush(context.getString(R.string.project_id), accessToken, push)
    }

    companion object {
        private const val APP_ID_REQUEST = "client/app_id"
        private const val SERVICE_ACCOUNT_FILE = "fcm_service.json"
        private const val GOOGLE_FCM_MESSAGE_URL = "https://www.googleapis.com/auth/firebase.messaging"
    }
}