package app.snappik.insureme.viewmodel.transformations

import android.content.Context
import android.text.format.DateFormat
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import app.snappik.insureme.R
import app.snappik.insureme.model.AlarmType
import app.snappik.insureme.model.Strategy
import app.snappik.insureme.model.UnlockType
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.utils.Utilities.cutSensitive
import java.util.Date

fun LiveData<Settings>.getFormattedTime(context: Context): LiveData<String> = Transformations.map(this) { settings ->
    if(settings.alarmType == AlarmType.DELAY){
        String.format(context.getString(R.string.delay_message), settings.delay / 1000 / 60)
    } else String.format(context.getString(R.string.settings_once_main), DateFormat.format("hh:mm a", Date(settings.time)).toString())
}

fun LiveData<Settings>.getFormattedStrategies(context: Context): LiveData<String> = Transformations.map(this) { settings ->
    if (settings.strategies.isNotEmpty()) settings.strategies.joinToString { it.name } else context.getString(R.string.general_none)
}

fun LiveData<Settings>.getFormattedEmailData(context: Context): LiveData<String> = Transformations.map(this) { settings ->
    val mEmail = if (settings.email.isEmpty()) context.getString(R.string.general_none) else settings.email.cutSensitive()
    val mNameSurname = if (settings.nameSurname.isEmpty()) context.getString(R.string.general_none) else settings.nameSurname.cutSensitive()
    String.format(context.getString(R.string.settings_change_sub_email), mEmail, mNameSurname)
}

fun LiveData<Settings>.getFormattedMessage(context: Context): LiveData<String> = Transformations.map(this) { settings ->
    if (settings.message.isEmpty()) context.getString(R.string.general_none) else settings.message.cutSensitive()
}

fun LiveData<Settings>.getFormattedPhoneNumber(context: Context): LiveData<String> = Transformations.map(this) { settings ->
    if (settings.destinationNumber.isEmpty()) context.getString(R.string.general_none) else settings.destinationNumber.cutSensitive()
}

fun LiveData<Settings>.getFormattedTimeExact(): LiveData<String> = Transformations.map(this) { settings ->
    DateFormat.format("hh:mm a", Date(settings.time)).toString()
}

fun LiveData<Settings>.getFormattedMinutesDelay(): LiveData<Long> = Transformations.map(this) { settings -> settings.delay / 1000 / 60 }

fun LiveData<Settings>.getPremium(): LiveData<Boolean> = Transformations.map(this) { settings -> settings.isPremium }

fun LiveData<Settings>.isSendLocation(): LiveData<Boolean> = Transformations.map(this) { settings -> settings.sendLocation }

fun LiveData<Settings>.isAttemptsRestricted(): LiveData<Boolean> = Transformations.map(this) { settings -> settings.amountOfAttempts > 0 }

fun LiveData<Settings>.isPanicPinEnabled(): LiveData<Boolean> = Transformations.map(this) { settings -> settings.panicPIN }

fun LiveData<Settings>.isPinEmpty(): LiveData<Boolean> = Transformations.map(this) { settings -> settings.pinLength <= 0 }

fun LiveData<Settings>.getPinLength(): LiveData<Int> = Transformations.map(this) { settings -> settings.pinLength }

fun LiveData<Settings>.isFallDetected(): LiveData<Boolean> = Transformations.map(this) { settings -> settings.fallDetector }

fun LiveData<Settings>.isPin(): LiveData<Boolean> = Transformations.map(this) { settings -> settings.unlockType == UnlockType.PIN }

fun LiveData<Settings>.getUnlockString(context: Context): LiveData<String?> = Transformations.map(this) {
        settings -> if(settings.unlockType.res != 0)  context.getString(settings.unlockType.res) else null
}

fun LiveData<Settings>.isDelay(): LiveData<Boolean> = Transformations.map(this) { settings -> settings.alarmType == AlarmType.DELAY }

fun LiveData<Settings>.getAlarmString(context: Context): LiveData<String?> = Transformations.map(this) {
        settings -> if(settings.alarmType.res != 0)  context.getString(settings.alarmType.res) else null
}

fun LiveData<Settings>.getState(): LiveData<Int> = Transformations.map(this) { settings ->
    settings.strategies.run {
        when {
            contains(Strategy.EMAIL) && contains(Strategy.PUSH) && contains(Strategy.SMS) -> R.id.email_push_sms
            contains(Strategy.EMAIL) && contains(Strategy.PUSH) -> R.id.email_push
            contains(Strategy.EMAIL) && contains(Strategy.SMS) -> R.id.email_sms
            contains(Strategy.SMS) && contains(Strategy.PUSH) -> R.id.push_sms
            contains(Strategy.SMS) -> R.id.sms
            contains(Strategy.EMAIL) -> R.id.email
            contains(Strategy.PUSH) -> R.id.push
            else -> R.id.nothing
        }
    }
}