package app.snappik.insureme.viewmodel.transformations

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import app.snappik.insureme.R
import app.snappik.insureme.model.entity.Friend

fun LiveData<List<Friend>>.toString(context: Context): LiveData<String> =
    Transformations.map(this) { list ->
        val devices = list?.filter { it.selected }?.map { it.displayName }?.joinToString { it }
        if (devices.isNullOrEmpty()) context.getString(R.string.general_none) else devices
    }