package app.snappik.insureme.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.snappik.insureme.model.AlarmType
import app.snappik.insureme.model.Strategy
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.NONE
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.PREPARE
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.PROCESSING
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.REQUIRED
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.TIME
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.FALL_DETECTED

class ActionViewModel : AbstractViewModel() {

    private val mTime = MutableLiveData(-1L)
    val time: LiveData<Long> = mTime

    private var currentUnlockAttempt = -1

    private val mAction = MutableLiveData(NONE)
    val action: LiveData<ActionStatus> = mAction

    fun isTicking(): Boolean {
        return action.value == REQUIRED || action.value == FALL_DETECTED || action.value == TIME
    }

    fun updateTime(time: Long) = mTime.postValue(time)

    fun timerFinished(settings: Settings) = mAction.postValue(when(action.value){
        TIME -> REQUIRED
        else -> if(settings.strategies.contains(Strategy.PUSH)) PREPARE else PROCESSING
    })

    fun unlockWithBiometrics(settings: Settings) = mAction.postValue(if(action.value == REQUIRED && settings.alarmType == AlarmType.DELAY) TIME else NONE)

    fun tryUnlockPin(pinMD5: String, settings: Settings) {
        val aAction: ActionStatus = when{
            pinMD5 == settings.pin && action.value == REQUIRED && settings.alarmType == AlarmType.DELAY -> TIME
            pinMD5 == settings.pin -> NONE
            pinMD5 == settings.reversedPin && settings.panicPIN && settings.strategies.contains(Strategy.PUSH) -> PREPARE
            pinMD5 == settings.reversedPin && settings.panicPIN -> PROCESSING
            settings.amountOfAttempts > 0 && currentUnlockAttempt == 0 && settings.strategies.contains(Strategy.PUSH) -> PREPARE
            settings.amountOfAttempts > 0 && currentUnlockAttempt == 0 -> PROCESSING
            settings.amountOfAttempts > 0 && currentUnlockAttempt > 0 -> {
                currentUnlockAttempt -= 1
                return
            }
            settings.amountOfAttempts > 0 && currentUnlockAttempt < 0 -> {
                currentUnlockAttempt = settings.amountOfAttempts
                return
            }
            else -> return
        }
        currentUnlockAttempt = -1
        mAction.postValue(aAction)
    }

    fun setAction(action: ActionStatus, force: Boolean = false) = if(force) mAction.value = action else mAction.postValue(action)

    enum class ActionStatus { NONE, TIME, FALL_DETECTED, REQUIRED, SETTINGS, PROCESSING, PREPARE }
}