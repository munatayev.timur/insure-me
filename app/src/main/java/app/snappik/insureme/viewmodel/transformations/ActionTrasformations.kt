package app.snappik.insureme.viewmodel.transformations

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import app.snappik.insureme.R
import app.snappik.insureme.viewmodel.ActionViewModel

fun LiveData<ActionViewModel.ActionStatus>.getState(): LiveData<Int> =
    Transformations.map(this) { action ->
        when (action) {
            ActionViewModel.ActionStatus.SETTINGS -> R.id.showSettings
            else -> R.id.hideSettings
        }
    }