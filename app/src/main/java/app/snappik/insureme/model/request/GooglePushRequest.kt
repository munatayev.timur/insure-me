package app.snappik.insureme.model.request

import app.snappik.insureme.utils.action.PushAction.Companion.CHANNEL_ID
import app.snappik.insureme.utils.action.PushAction.Companion.LATITUDE
import app.snappik.insureme.utils.action.PushAction.Companion.LONGITUDE

data class GooglePushRequest(val message: Message) {
    class Message(val topic: String, val notification: Notification?, val data: Map<String, String>?){
        class Notification(val body: String, val title: String)
    }

    companion object{
        fun createToDelete(tokenToReceive: String, tokenToDelete: String) = GooglePushRequest(
            Message(
                topic = tokenToReceive,
                notification = null,
                data = mapOf(HuaweiPushRequest.DELETE_KEY to tokenToDelete)
            ))

        fun create(topic: String, title: String, body: String, lat: String? = null, lon: String? = null) = GooglePushRequest(
            Message(
            topic = topic,
            notification = Message.Notification(body, title),
                data = hashMapOf(CHANNEL_ID to topic).apply {
                    if(lat != null && lon != null){
                       put(LATITUDE, lat)
                        put(LONGITUDE, lon)
                    }
                }
        ))
    }
}