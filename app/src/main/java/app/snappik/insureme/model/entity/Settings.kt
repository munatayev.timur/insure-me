package app.snappik.insureme.model.entity

import android.Manifest
import android.content.Context
import android.location.LocationManager
import android.os.Build
import androidx.room.Entity
import androidx.room.PrimaryKey
import app.snappik.insureme.R
import app.snappik.insureme.model.AlarmType
import app.snappik.insureme.model.Strategy
import app.snappik.insureme.model.UnlockType

fun settings(block: Settings.() -> Unit): Settings = Settings().apply(block)

@Entity(tableName = "settings")
data class Settings(
    @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
    var isPremium: Boolean = false,
    var sendLocation: Boolean = false,
    var nameSurname: String = "",
    var email: String = "",
    var amountOfAttempts: Int = -1,
    var panicPIN: Boolean = false,
    var delay: Long = 1000 * 60 * 30L,
    var time: Long = System.currentTimeMillis(),
    var strategies: HashSet<Strategy> = hashSetOf(Strategy.SMS),
    var destinationNumber: String = "",
    var unlockType: UnlockType = UnlockType.PIN,
    var pin: String = "",
    var reversedPin: String = "",
    var pinLength: Int = 0,
    var message: String = "",
    var alarmType: AlarmType = AlarmType.DELAY,
    var fallDetector: Boolean = false
) {
    fun getIncorrectSettings(isFingerprintSupported: Boolean, selectedFriends: List<Friend>): HashSet<Int> = HashSet<Int>().apply {
        if (strategies.contains(Strategy.SMS) && (destinationNumber.isEmpty() || isPhoneNumberCorrect())) add(R.string.settings_incorrect_phone_number)
        if (strategies.contains(Strategy.SMS) && message.isEmpty()) add(R.string.settings_incorrect_sms_message)
        if (unlockType == UnlockType.PIN && pin.isEmpty()) add(R.string.settings_incorrect_pin)
        if (unlockType == UnlockType.BIOMETRIC && isFingerprintSupported.not()) add(R.string.settings_incorrect_biometrics)
        if (alarmType == AlarmType.ONCE && time < System.currentTimeMillis()) add(R.string.settings_incorrect_once_time)
        if (strategies.contains(Strategy.EMAIL) && (email.isEmpty() || nameSurname.isEmpty())) add(R.string.settings_incorrect_email_data)
        if (strategies.contains(Strategy.EMAIL) && message.isEmpty()) add(R.string.settings_incorrect_sms_message)
        if (strategies.isEmpty()) add(R.string.settings_empty_strategies)
        if(strategies.contains(Strategy.PUSH) && selectedFriends.isEmpty()) add(R.string.settings_no_friends_selected)
    }

    fun getExactTime() =
            if (alarmType == AlarmType.DELAY) System.currentTimeMillis() + delay else time

    fun getNonExactTime() = if(alarmType == AlarmType.DELAY) delay else time - System.currentTimeMillis()

    fun getPermissions(): List<String> = arrayListOf<String>().apply {
        if (strategies.contains(Strategy.SMS)) add(Manifest.permission.SEND_SMS)
        if (sendLocation) add(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    fun isPossibleToReversePIN() = pin != reversedPin

    private fun isPhoneNumberCorrect(): Boolean = destinationNumber.length < 9 || destinationNumber.length > 12

    fun isLocationServiceEnabled(context: Context): Boolean =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                val lm = context.getSystemService(Context.LOCATION_SERVICE) as? LocationManager
                lm?.isLocationEnabled ?: false
            } else {
                val mode: Int = android.provider.Settings.Secure.getInt(
                        context.contentResolver, android.provider.Settings.Secure.LOCATION_MODE,
                        android.provider.Settings.Secure.LOCATION_MODE_OFF
                )
                mode != android.provider.Settings.Secure.LOCATION_MODE_OFF
            }

    override fun toString(): String {
        return "Settings(sendLocation=$sendLocation, amountOfAttempts=$amountOfAttempts, panicPIN=$panicPIN, delay=$delay, time=$time, strategies=$strategies, unlockType=$unlockType, alarmType=$alarmType)"
    }
}