package app.snappik.insureme.model

import app.snappik.insureme.R
import app.snappik.insureme.model.entity.Settings

enum class AlarmType(val res: Int, val desc: Int, val isPremium: Boolean) {
    DELAY(R.string.alarm_type_delay, R.string.alarm_type_delay_desc,false),
    ONCE(R.string.alarm_type_once, R.string.alarm_type_once_desc, true);

    companion object{
        fun findByRes(res: Int): AlarmType = values().find { it.res == res } ?: DELAY
        fun all() = values().sorted().map { it.res }
        fun description() = values().sorted().map { it.desc }
        fun disabled(settings: Settings) = if(settings.isPremium) emptyList() else values().filter { it.isPremium }.map { it.res }
    }
}