package app.snappik.insureme.model

data class BillingProduct(
    val title: String,
    val description: String,
    val price: String,
    val original: Any
)