package app.snappik.insureme.model.entity

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

fun friend(block: Friend.() -> Unit): Friend = Friend().apply(block)

@Entity(tableName = "friend", indices = [Index(value = ["displayName"], unique = true)])
data class Friend(@PrimaryKey(autoGenerate = true)
                  val id:Long = 0,
                  var displayName: String = "",
                  var selected: Boolean = false,
                  var token: String = "")