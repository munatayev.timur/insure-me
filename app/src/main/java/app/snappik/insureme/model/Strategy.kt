package app.snappik.insureme.model

import app.snappik.insureme.R
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.model.entity.User

enum class Strategy(val res: Int, val desc: Int, val isPremium: Boolean, val isGuestAvailable: Boolean) {
    SMS(R.string.strategy_sms, R.string.strategy_sms_desc, false, true),
    EMAIL(R.string.strategy_email, R.string.strategy_sms_and_email_desc, true, true),
    PUSH(R.string.strategy_push, R.string.strategy_push_desc, false, false);

    companion object{
        fun findByRes(res: Int): Strategy = values()
            .find { it.res == res } ?: SMS

        fun all() = values().sorted().map { it.res }
        fun description() = values().sorted().map { it.desc }
        fun disabledPremium(settings: Settings) = if(settings.isPremium) emptyList() else values().filter { it.isPremium }.map { it.res }
        fun disabledGuest(user: User) = if(user.type != User.Type.GUEST) emptyList() else values().filter { it.isGuestAvailable.not() }.map { it.res }
    }
}