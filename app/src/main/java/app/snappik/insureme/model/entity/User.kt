package app.snappik.insureme.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

fun user(block: User.() -> Unit): User = User().apply(block)

@Entity(tableName = "user")
data class User(var displayName: String = "",
                var type: Type = Type.GUEST,
                @PrimaryKey(autoGenerate = true)
                val id:Long = 0,
                var isPremium: Boolean = false,
                var isAgreedWithTerms: Boolean = false){

    enum class Type { GUEST, GOOGLE, HUAWEI }
}