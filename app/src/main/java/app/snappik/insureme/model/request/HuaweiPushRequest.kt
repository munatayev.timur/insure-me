package app.snappik.insureme.model.request

class HuaweiPushRequest(val message: Message, val validate_only: Boolean = false) {
    class Message(val android: Android?, val topic: String, val data: Map<String, String>?) {
        class Android(val notification: Notification, val data: Map<String, String>? = null) {
            class Notification(
                val title: String? = null,
                val body: String? = null,
                val foreground_show: Boolean = true,
                val click_action: ClickAction? = null,
                val channel_id: String? = null,
            ) {
                class ClickAction(val type: Int, val intent: String)
            }
        }
    }

    companion object {
        fun createForDelete(tokenToReceive: String, tokenToDelete: String) = HuaweiPushRequest(
            message = Message(
                data = mapOf(DELETE_KEY to tokenToDelete),
                topic = tokenToReceive,
                android = null
            )
        )

        fun createFrom(title: String, body: String, topic: String, intent: String) = HuaweiPushRequest(
            message = Message(
                data = null,
                topic = topic,
                android = Message.Android(
                    notification = Message.Android.Notification(
                        title = title,
                        body = body,
                        channel_id = topic,
                        click_action = Message.Android.Notification.ClickAction(
                            type = 1,
                            intent = intent
                        )
                    )
                )
            )
        )

        const val DELETE_KEY = "delete"
    }
}