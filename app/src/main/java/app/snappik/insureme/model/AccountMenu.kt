package app.snappik.insureme.model

import android.content.Context
import app.snappik.insureme.R

enum class AccountMenu(private val resId: Int) {
    FRIENDS(R.string.account_menu_friends),
    SETTINGS(R.string.account_menu_settings),
    ABOUT(R.string.account_menu_about),
    LOGOUT(R.string.account_menu_logout);

    companion object {
        fun flattenDisplayName(displayName: String, context: Context): List<String> =
            listOf(listOf(displayName.substring(0, kotlin.math.min(15, displayName.length))), values()
                .map { context.getString(it.resId) })
                .flatten()
    }
}