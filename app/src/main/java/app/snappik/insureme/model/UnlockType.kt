package app.snappik.insureme.model

import app.snappik.insureme.R
import app.snappik.insureme.model.entity.Settings

enum class UnlockType(val res: Int, val desc: Int, val isPremium: Boolean) {
    PIN(R.string.unlock_type_pin, R.string.unlock_type_pin_desc,false),
    BIOMETRIC(R.string.unlock_type_biometric,  R.string.unlock_type_biometric_desc, true);

    companion object {
        fun findByRes(res: Int): UnlockType = values().find { it.res == res } ?: PIN
        fun all() = values().sorted().map { it.res }
        fun description() = values().sorted().map { it.desc }
        fun disabled(settings: Settings) = if(settings.isPremium) emptyList() else values().filter { it.isPremium }.map { it.res }
    }
}