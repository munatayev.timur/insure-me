package app.snappik.insureme

import android.app.Activity
import android.app.Application
import android.os.Bundle
import app.snappik.insureme.common.AbstractActivity
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class InsureMeApplication : Application(), Application.ActivityLifecycleCallbacks {

    var currentActivity: AbstractActivity? = null

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(this)
    }

    override fun onActivityCreated(activity: Activity, p1: Bundle?) = Unit

    override fun onActivityStarted(activity: Activity) = Unit

    override fun onActivityResumed(activity: Activity) { if(activity is AbstractActivity) currentActivity = activity }

    override fun onActivityPaused(activity: Activity) = Unit

    override fun onActivitySaveInstanceState(activity: Activity, p1: Bundle) = Unit

    override fun onActivityStopped(activity: Activity){ if(currentActivity == activity) currentActivity = null }

    override fun onActivityDestroyed(activity: Activity){ if(currentActivity == activity) currentActivity = null }
}

