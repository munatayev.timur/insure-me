package app.snappik.insureme.service

import app.snappik.insureme.hilt.PushTokenHuawei
import app.snappik.insureme.model.request.HuaweiPushRequest
import app.snappik.insureme.utils.push.IPushToken
import app.snappik.insureme.viewmodel.FriendViewModel
import app.snappik.insureme.viewmodel.UserViewModel
import com.huawei.hms.push.HmsMessageService
import com.huawei.hms.push.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HmsService : HmsMessageService() {

    @Inject @PushTokenHuawei
    lateinit var pushToken: IPushToken

    @Inject
    lateinit var userViewModel: UserViewModel

    @Inject lateinit var friendViewModel: FriendViewModel

    override fun onNewToken(newToken: String?) {
        super.onNewToken(newToken)
        userViewModel.user?.value?.let {
            pushToken.subscribeToTopic(it)
        }
    }

    override fun onMessageReceived(rm: RemoteMessage) {
        if(rm.dataOfMap.contains(HuaweiPushRequest.DELETE_KEY)){
            rm.dataOfMap[HuaweiPushRequest.DELETE_KEY]?.let { friendViewModel.deleteByToken(it) }
        }
    }
}