package app.snappik.insureme.service

import android.annotation.SuppressLint
import android.os.Bundle
import app.snappik.insureme.hilt.MessageNotify
import app.snappik.insureme.model.request.HuaweiPushRequest.Companion.DELETE_KEY
import app.snappik.insureme.utils.action.PushAction.Companion.BODY
import app.snappik.insureme.utils.action.PushAction.Companion.CHANNEL_ID
import app.snappik.insureme.utils.action.PushAction.Companion.DISPLAY_NAME
import app.snappik.insureme.utils.action.PushAction.Companion.LATITUDE
import app.snappik.insureme.utils.action.PushAction.Companion.LONGITUDE
import app.snappik.insureme.utils.notification.INotification
import app.snappik.insureme.utils.notification.MessageNotification
import app.snappik.insureme.viewmodel.FriendViewModel
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
@AndroidEntryPoint
class FirebaseService : FirebaseMessagingService() {

    @Inject @MessageNotify lateinit var notification: INotification

    @Inject lateinit var friendViewModel: FriendViewModel

    override fun onMessageReceived(rm: RemoteMessage) {
        if(rm.data.contains(DELETE_KEY)){
            rm.data[DELETE_KEY]?.let { friendViewModel.deleteByToken(it) }
            return
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notification.createNotificationChannel()
        }

        val mNotification = notification as? MessageNotification
        val title = rm.notification?.title
        val body = rm.notification?.body
        if(title != null && body != null){
            notification.pendingIntentBundle = Bundle().apply {
                rm.data[LATITUDE]?.let { putString(LATITUDE, it) }
                rm.data[LONGITUDE]?.let { putString(LONGITUDE, it) }
                putString(DISPLAY_NAME, title)
                putString(BODY, body)
            }
            mNotification?.setText(title, body)
            rm.data[CHANNEL_ID]?.let { notification.channelId = it }
            notification.showNotification()
        }
    }
}