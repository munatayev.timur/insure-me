package app.snappik.insureme.service

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import androidx.lifecycle.LifecycleService
import app.snappik.insureme.hilt.FallNotify
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.observer.ISettingsObserver
import app.snappik.insureme.receiver.BootReceiver
import app.snappik.insureme.ui.main.EventsFragment
import app.snappik.insureme.utils.AlarmHelper
import app.snappik.insureme.utils.Utilities
import app.snappik.insureme.utils.notification.INotification
import app.snappik.insureme.viewmodel.ActionViewModel
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.FALL_DETECTED
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.REQUIRED
import app.snappik.insureme.viewmodel.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.DecimalFormat
import javax.inject.Inject
import kotlin.math.pow
import kotlin.math.sqrt

@AndroidEntryPoint
class FallDetectorService : LifecycleService(), ISettingsObserver, SensorEventListener {

    @Inject @FallNotify lateinit var notification: INotification

    @Inject override lateinit var settingsViewModel: SettingsViewModel

    @Inject lateinit var actionViewModel: ActionViewModel

    @Inject lateinit var alarmHelper: AlarmHelper

    private val sensorManager: SensorManager by lazy { getSystemService(SENSOR_SERVICE) as SensorManager }

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) notification.createNotificationChannel()
        notification.showNotification()
        subscribeToSettingsUpdate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onSettingsFetched(settings: Settings) {
        if(settings.fallDetector.not()){
            alarmHelper.removeAlarm(BootReceiver.ACTION_FALL_DETECTOR, BootReceiver::class.java)
            removeListener()
            stopSelf()
        } else addListener()
    }

    private fun addListener(){
        if (sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).isNotEmpty()) {
            val sensor: Sensor = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER)[0]
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    private fun removeListener(){
        if (sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).isNotEmpty()) {
            val sensor: Sensor = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER)[0]
            sensorManager.unregisterListener(this, sensor)
        }
    }

    override fun onSensorChanged(sensorEvent: SensorEvent) {

        val loX = sensorEvent.values[0]
        val loY = sensorEvent.values[1]
        val loZ = sensorEvent.values[2]

        val loAccelerationReader = sqrt(loX.toDouble().pow(2.0)
                    + loY.toDouble().pow(2.0)
                    + loZ.toDouble().pow(2.0))

        val precision = DecimalFormat("0.00")
        val ldAccRound: Double = precision.format(loAccelerationReader).toDouble()

        if (ldAccRound > 0.3 && ldAccRound < 0.8) {
            actionViewModel.action.value?.let {
                if(it != REQUIRED && it != FALL_DETECTED) {
                    val intent = Intent(this, AlarmService::class.java)
                    actionViewModel.setAction(FALL_DETECTED)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(intent)
                    } else startService(intent)
                }
            }
        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int)  = Unit

    companion object {
        fun checkAndStart(context: Context) {
            AlarmHelper(context).setUpAlarm(
                System.currentTimeMillis() + EventsFragment.CHECK_FALL_DETECTOR_TIME,
                BootReceiver.ACTION_FALL_DETECTOR, BootReceiver::class.java
            )
            if(Utilities.isServiceRunning(context, FallDetectorService::class.java).not()){
                val intent = Intent(context, FallDetectorService::class.java)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(intent)
                } else context.startService(intent)
            }
        }
    }
}