package app.snappik.insureme.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.CountDownTimer
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.lifecycle.LifecycleService
import app.snappik.insureme.InsureMeApplication
import app.snappik.insureme.hilt.TimerNotify
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.observer.IActionObserver
import app.snappik.insureme.observer.IFriendObserver
import app.snappik.insureme.observer.ISettingsObserver
import app.snappik.insureme.observer.IUserObserver
import app.snappik.insureme.receiver.AlarmReceiver
import app.snappik.insureme.receiver.AlarmReceiver.Companion.ACTION_RING
import app.snappik.insureme.ui.main.MainActivity
import app.snappik.insureme.utils.ActionHelper
import app.snappik.insureme.utils.AlarmHelper
import app.snappik.insureme.utils.analytics.IAnalytics
import app.snappik.insureme.utils.auth.AuthManager
import app.snappik.insureme.utils.notification.INotification
import app.snappik.insureme.utils.notification.TimerNotification
import app.snappik.insureme.utils.wakelock.WakeLockManager
import app.snappik.insureme.viewmodel.ActionViewModel
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.FALL_DETECTED
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.PROCESSING
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.REQUIRED
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.TIME
import app.snappik.insureme.viewmodel.FriendViewModel
import app.snappik.insureme.viewmodel.SettingsViewModel
import app.snappik.insureme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AlarmService : LifecycleService(), IActionObserver, ISettingsObserver, IFriendObserver, IUserObserver {

    @Inject lateinit var authManager: AuthManager

    @Inject lateinit var analytics: IAnalytics

    @Inject override lateinit var actionViewModel: ActionViewModel

    @Inject override lateinit var settingsViewModel: SettingsViewModel

    @Inject override lateinit var userViewModel: UserViewModel

    @Inject override lateinit var friendViewModel: FriendViewModel

    @Inject lateinit var wakeLockManager: WakeLockManager

    @Inject lateinit var alarmHelper: AlarmHelper

    @Inject lateinit var actionHelper: ActionHelper

    @Inject @TimerNotify lateinit var notification: INotification

    private val alarmBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(c: Context?, intent: Intent?) {
            intent?.let {
                alarmHelper.removeAlarm(AlarmReceiver.ACTION_FIRE)
                wakeLockManager.releaseTransitionWakeLock(it)
                wakeLockManager.acquireServiceLock()
            }
        }
    }

    private var timer: CountDownTimer? = null

    private val vibrator by lazy {
        getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    }

    private var timerFiredTime: Pair<Long, Long> = Pair(-1, -1)

    private val ringtone by lazy {
        val alarmSound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        RingtoneManager.getRingtone(this, alarmSound)
    }

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) notification.createNotificationChannel()
        notification.showNotification()
        fetchSettings()
        subscribeToUserUpdate()
        subscribeToFriendsUpdate()
        registerReceiver(alarmBroadcastReceiver, IntentFilter(ACTION_RING))
    }

    override fun onSettingsFetched(settings: Settings) {
        alarmHelper.setUpAlarm(settings.getExactTime(), AlarmReceiver.ACTION_FIRE)
        if(actionViewModel.action.value != REQUIRED && actionViewModel.action.value != FALL_DETECTED){
            actionViewModel.setAction(TIME, true)
        }
        subscribeToActionUpdate(settings)
    }

    override fun onUserFetched(user: User) {
        actionHelper.user = user
    }

    override fun onFriendsFetched(list: List<Friend>) {
        actionHelper.friends = list
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onDestroy() {
        unregisterReceiver(alarmBroadcastReceiver)
        alarmHelper.removeAlarm(AlarmReceiver.ACTION_FIRE)
        ringtone.stop()
        vibrator.cancel()
        timer?.cancel()
        super.onDestroy()
    }

    private fun openMain() {
        (application as? InsureMeApplication)?.let { if(it.currentActivity is MainActivity) return }
        val dialogIntent = Intent(this, MainActivity::class.java)
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(dialogIntent)
    }

    private fun playSound() {
        ringtone?.play()
        val mVibratePattern = longArrayOf(0, 400, 800, 600, 800)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createWaveform(mVibratePattern, 0))
        } else vibrator.vibrate(1000)
    }

    private fun turnOffSound(){
        ringtone.stop()
        vibrator.cancel()
        timer?.cancel()
    }

    override fun shutDownService() {
        turnOffSound()
        stopSelf()
    }

    override fun startTimer(time: Long, settings: Settings) {
        turnOffSound()
        val timerNotification = notification as? TimerNotification
        timerNotification?.listener?.onActionRequired(false)
        timer = object : CountDownTimer(time, INTERVAL) {
            override fun onFinish() = actionViewModel.timerFinished(settings).let { Unit }
            override fun onTick(millisUntilFinished: Long) {
                actionViewModel.updateTime(millisUntilFinished)
                timerNotification?.listener?.onTimeUpdated(millisUntilFinished)
            }
        }.start()
    }

    override fun actionRequired() {
        val timerNotification = notification as? TimerNotification
        analytics.alarmFired(timerFiredTime.first, System.currentTimeMillis(), timerFiredTime.second)
        timerNotification?.listener?.onActionRequired(true)
        playSound()
        openMain()
    }

    override fun doAction(settings: Settings) {
        turnOffSound()
        actionHelper.doAction(settings){
            stopSelf()
        }
    }

    override fun prepareAction(){
        authManager.getAuth()?.silentSignIn {
            actionHelper.token = it
            actionViewModel.setAction(PROCESSING)
        }
    }

    companion object {
        private const val INTERVAL = 1000L
    }
}