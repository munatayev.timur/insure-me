package app.snappik.insureme.utils.biometric

import android.content.Context
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.fragment.app.Fragment
import app.snappik.insureme.R
import java.util.concurrent.Executor
import javax.inject.Inject

class BiometricGoogle @Inject constructor(private val context: Context) : IBiometric{

    override var onSuccess: () -> Unit = {}

    override fun auth(fragment: Fragment, executor: Executor) {
        val prompt = BiometricPrompt(fragment, executor, object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                onSuccess.invoke()
            }
        })

        val dialog = BiometricPrompt.PromptInfo
            .Builder()
            .setTitle(fragment.getString(R.string.biometric_title))
            .setDescription(fragment.getString(R.string.biometric_description))
            .setNegativeButtonText(fragment.getString(R.string.general_cancel))
            .build()

        prompt.authenticate(dialog)
    }

    override fun canAuthWithFingerprint(): Boolean {
        return BiometricManager.from(context).canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK) == BiometricManager.BIOMETRIC_SUCCESS
    }
}