package app.snappik.insureme.utils.view

import android.content.Context
import android.util.AttributeSet
import app.snappik.insureme.BR
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractLayoutDataBinding
import app.snappik.insureme.databinding.ItemLayoutMenuBinding

class MenuItemView : AbstractLayoutDataBinding<ItemLayoutMenuBinding> {

    constructor(context: Context) : super(context)

    constructor(context: Context, attr: AttributeSet) : super(context, attr)

    constructor(context: Context, attr: AttributeSet, i: Int) : super(context, attr, i)

    override fun getViewId(): Int = R.layout.item_layout_menu

    fun onExpand(){
        BR.expandTip.updateWith { expandTip = expandTip.not() }
    }

    fun setOnClick(runnable: Runnable) {
        BR.onClickRunnable.updateWith { onClickRunnable = runnable }
    }

    fun setAvailabilityAlpha(alpha: Float) {
        BR.availabilityAlpha.updateWith { availabilityAlpha = alpha }
    }

    fun setTipText(text: String) {
        BR.tipText.updateWith { tipText = text }
    }

    fun setTitleText(text: String) {
        BR.titleText.updateWith { titleText = text }
    }

    fun setSubText(text: String) {
        BR.subText.updateWith { subText = text }
    }

    override fun ItemLayoutMenuBinding.initVariables() {
        layout = this@MenuItemView
        onClickRunnable = Runnable {}
        availabilityAlpha = 1F
        tipText = ""
        subText = ""
        titleText = ""
    }
}