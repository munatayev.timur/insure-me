package app.snappik.insureme.utils.nearby

import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.ui.dialog.InfoDialog

interface INearby {
    val activity: FragmentActivity
    fun startAdvertising(currentDeviceName: String, currentDeviceToken: String?,  onSuccess: (() -> Unit)? = null,
                         onDevicePaired: (remoteDeviceName: String, remoteDeviceToken: String) -> Unit)
    fun startDiscovery(onScannerStart: () -> Unit, onDeviceFound:(endpointId: String, name: String) -> Unit,
                       onDeviceLost:(endpointId: String) -> Unit)
    fun connect(entryId: String, onDevicePaired: (remoteDeviceName: String, remoteDeviceToken: String) -> Unit)
    fun stopAdvertising()
    fun stopDiscovering()
    fun disconnectAll()
    fun Exception.showError(title: String) = InfoDialog(title, message ?: "Not details").show(activity.supportFragmentManager)
}