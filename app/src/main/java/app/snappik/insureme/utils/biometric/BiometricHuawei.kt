package app.snappik.insureme.utils.biometric

import android.content.Context
import androidx.fragment.app.Fragment
import app.snappik.insureme.R
import com.huawei.hms.support.api.fido.bioauthn.BioAuthnCallback
import com.huawei.hms.support.api.fido.bioauthn.BioAuthnManager
import com.huawei.hms.support.api.fido.bioauthn.BioAuthnPrompt
import com.huawei.hms.support.api.fido.bioauthn.BioAuthnResult
import java.util.concurrent.Executor
import javax.inject.Inject

class BiometricHuawei @Inject constructor(private val context: Context) : IBiometric {

    override var onSuccess: () -> Unit = {}

    override fun auth(fragment: Fragment, executor: Executor) {

        val prompt = BioAuthnPrompt(fragment, executor, object : BioAuthnCallback() {
            override fun onAuthSucceeded(p0: BioAuthnResult) {
                super.onAuthSucceeded(p0)
                onSuccess.invoke()
            }
        })

        val dialog = BioAuthnPrompt.PromptInfo.Builder()
            .setTitle(fragment.getString(R.string.biometric_title))
            .setDescription(fragment.getString(R.string.biometric_description))
            .setNegativeButtonText(fragment.getString(R.string.general_cancel))
            .build()

        prompt.auth(dialog)
    }

    override fun canAuthWithFingerprint(): Boolean {
        return BioAuthnManager(context).canAuth() == BioAuthnManager.BIO_AUTHN_SUCCESS
    }
}