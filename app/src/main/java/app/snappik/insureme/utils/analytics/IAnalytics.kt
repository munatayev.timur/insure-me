package app.snappik.insureme.utils.analytics

import android.os.Bundle
import app.snappik.insureme.model.entity.Settings
import java.util.Date

interface IAnalytics {

    fun alarmFired(start: Long, finished: Long, delay: Long){
        onEvent(ALARM_EVENT, Bundle().apply {
            putString(ALARM_START_ITEM, Date(start).toString())
            putString(ALARM_FINISHED_ITEM, Date(finished).toString())
            putString(ALARM_DELAY_ITEM, delay.toString())
        })
    }

    fun settingsUpdated(settings : Settings){
        onEvent(SETTINGS_EVENT, Bundle().apply {
            putString(NEW_SETTINGS, settings.toString())
        })
    }

    fun tryToBuy() = onEvent(BUY_EVENT, Bundle().apply {
        putString(SUBSCRIPTION_CLICKED, "Subscribe clicked")
    })

    fun subscriptionRead() = onEvent(SUBSCRIPTION_READ_EVENT, Bundle().apply {
        putString(SUBSCRIPTION_ITEM, "Subscription read")
    })

    fun onEvent(type: String, bundle: Bundle)

    companion object {
        private const val SUBSCRIPTION_READ_EVENT = "SubscriptionReadEvent"
        private const val SUBSCRIPTION_ITEM = "SubscriptionObserved"

        private const val ALARM_EVENT = "AlarmStarted"
        private const val ALARM_START_ITEM = "AlarmStartTime"
        private const val ALARM_FINISHED_ITEM = "AlarmFinishedTime"
        private const val ALARM_DELAY_ITEM = "AlarmDelayTime"

        private const val SETTINGS_EVENT = "SettingsUpdated"
        private const val NEW_SETTINGS = "NewSettings"

        private const val BUY_EVENT = "BuyClicked"
        private const val SUBSCRIPTION_CLICKED = "SubscriptionClicked"
    }
}