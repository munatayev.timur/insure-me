package app.snappik.insureme.utils.map

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.R
import com.huawei.agconnect.config.AGConnectServicesConfig
import com.huawei.hms.maps.CameraUpdateFactory
import com.huawei.hms.maps.MapView
import com.huawei.hms.maps.model.LatLng
import com.huawei.hms.maps.model.MarkerOptions
import javax.inject.Inject

class MapHUAWEI @Inject constructor(private val activity: FragmentActivity): IMap {

    override val mapView: MapView = MapView(activity)

    override fun loadMap(lat: String, lon: String) {
        mapView.getMapAsync {
            val locate = LatLng(lat.toDouble(), lon.toDouble())
            val markerString = activity.getString(R.string.location_last_known_location)
            val marker = MarkerOptions()
                .position(locate)
                .title(markerString)
            it.addMarker(marker)
            it.animateCamera(CameraUpdateFactory.newLatLngZoom(locate, 12.0f))
        }
    }

    override fun onStart() {
        mapView.onStart()
    }

    override fun onStop() {
        mapView.onStop()
    }

    override fun onCreate() {
        mapView.onCreate(Bundle().apply {
            putString(IMap.MAP_BUNDLE_KEY, AGConnectServicesConfig.fromContext(activity).getString("client/api_key"))
        })
    }

    override fun onResume() {
        mapView.onResume()
    }

    override fun onPause() {
        mapView.onPause()
    }

    override fun onLowMemory() {
        mapView.onLowMemory()
    }

    override fun onDestroy() {
        mapView.onDestroy()
    }
}