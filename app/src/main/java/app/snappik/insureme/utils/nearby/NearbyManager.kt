package app.snappik.insureme.utils.nearby

import app.snappik.insureme.model.entity.User
import javax.inject.Inject

class NearbyManager @Inject constructor(private val huawei: INearby, private val google: INearby) {

    var user: User? = null

    fun getNearby() : INearby? = when (user?.type ?: User.Type.GUEST) {
        User.Type.GOOGLE -> google
        User.Type.HUAWEI -> huawei
        else -> null
    }
}