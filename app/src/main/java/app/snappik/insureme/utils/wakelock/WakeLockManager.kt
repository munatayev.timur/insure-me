package app.snappik.insureme.utils.wakelock

import android.content.Context
import android.content.Intent
import android.os.PowerManager
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

class WakeLockManager @Inject constructor(context: Context) : Wakelocks {

    private val powerManager by lazy { context.getSystemService(Context.POWER_SERVICE) as PowerManager }

    private val wakelockCounter = AtomicInteger(0)
    private val wakeLockIds = CopyOnWriteArrayList<Int>()

    private val serviceWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKE_LOCK_WRAPPER)
    private val transitionWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKE_LOCK_PUSHER)

    override fun acquireServiceLock() = serviceWakelock.acquire(60 * 60_000)

    override fun releaseServiceLock() {
        if (serviceWakelock.isHeld) {
            serviceWakelock.release()
        }
    }

    fun acquireTransitionWakeLock(intent: Intent) {
        transitionWakelock.acquire(60 * 1000)
        wakelockCounter.incrementAndGet().also { count ->
            wakeLockIds.add(count)
            intent.putExtra(COUNT, count)
        }
    }

    fun releaseTransitionWakeLock(intent: Intent) {
        val count = intent.getIntExtra(COUNT, -1)
        val wasRemoved = wakeLockIds.remove(count)
        if (wasRemoved && transitionWakelock.isHeld) {
            transitionWakelock.release()
        }
    }

    companion object {
        private const val COUNT = "WakeLockManager.COUNT"
        private const val WAKE_LOCK_WRAPPER = "InsureMe:AlertServiceWrapper"
        private const val WAKE_LOCK_PUSHER = "InsureMe:AlertServicePusher"
    }
}
