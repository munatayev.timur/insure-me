package app.snappik.insureme.utils

import android.app.ActivityManager
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import app.snappik.insureme.R
import java.math.BigInteger
import java.security.MessageDigest
import java.util.concurrent.TimeUnit


object Utilities {

    fun showKeyboard(view: View) {
        view.requestFocus()
        val imm: InputMethodManager? = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    fun Context.timeToString(time: Long): String{
        val h = TimeUnit.MILLISECONDS.toHours(time)
        val m = TimeUnit.MILLISECONDS.toMinutes(time)
        val deltaM = m - TimeUnit.HOURS.toMinutes(h)
        val s = TimeUnit.MILLISECONDS.toSeconds(time)
        val deltaS = s - TimeUnit.MINUTES.toSeconds(m)
        return if (h != 0L) {
            String.format(getString(R.string.default_count_down_hour_min_sec), h, deltaM, deltaS)
        } else if (h == 0L && deltaM != 0L) {
            String.format(getString(R.string.default_count_down_min_sec), deltaM, deltaS)
        } else {
            String.format(getString(R.string.default_count_down_sec), deltaS)
        }
    }

    fun String.getMD5() : String{
        val md = MessageDigest.getInstance("MD5")
        val bigInt = BigInteger(1, md.digest(this.toByteArray(Charsets.UTF_8)))
        return String.format("%032x", bigInt)
    }

    fun String.cutSensitive(): String = if(length < 4) this else substring(0, length / 4) + "***" + substring(
        length - length / 4,
        length
    )

    fun isServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}