package app.snappik.insureme.utils.push

import app.snappik.insureme.model.entity.User

interface IPushToken {
    fun initSubscription(user: User, success: (() -> Unit)? = null, failure: ((info: String) -> Unit)? = null)
    fun subscribeToTopic(user: User, success: (() -> Unit)? = null, failure: ((info: String) -> Unit)? = null)
    fun unsubscribeToTopic(user: User)
}