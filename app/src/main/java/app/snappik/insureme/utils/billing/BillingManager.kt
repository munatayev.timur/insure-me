package app.snappik.insureme.utils.billing

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.snappik.insureme.model.BillingProduct
import app.snappik.insureme.utils.billing.IBilling.Companion.REQ_CODE_BUY

typealias SubscriptionStatusListener = (isPremium: Boolean) -> Unit

class BillingManager(private val billingMachine : IBilling<*, *, *>) {

    var subscriptionListener : SubscriptionStatusListener? = null

    init {
        billingMachine.success = fun (){
            subscriptionListener?.invoke(true)
            mBillingFinished.postValue(true)
        }
    }

    private val mSubscriptionDetails = MutableLiveData<BillingProduct>(null)
    val subscriptionDetails: LiveData<BillingProduct> = mSubscriptionDetails

    private val mBillingFinished = MutableLiveData(false)
    val billingFinished: LiveData<Boolean> = mBillingFinished

    fun initConnection() = billingMachine.preProcess { checkAlreadyOwned() }

    private fun checkAlreadyOwned(){
        billingMachine.checkIsAlreadyOwn { alreadyOwn ->
            subscriptionListener?.invoke(alreadyOwn)
            if(alreadyOwn.not()) getProductInfo()
        }
    }

    private fun getProductInfo() = billingMachine.getProductInfo {
        mSubscriptionDetails.postValue(it)
    }

    fun purchase(product: BillingProduct) = billingMachine.requestPurchase(product)

    fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?){
        if(requestCode == REQ_CODE_BUY && billingMachine is BillingHUAWEI && resultCode == Activity.RESULT_OK) billingMachine.handlePurchase(intent)
    }
}