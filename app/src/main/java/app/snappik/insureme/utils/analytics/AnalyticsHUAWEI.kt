package app.snappik.insureme.utils.analytics

import android.content.Context
import android.os.Bundle
import com.huawei.hms.analytics.HiAnalytics
import javax.inject.Inject

class AnalyticsHUAWEI @Inject constructor(context: Context) : IAnalytics {

    private val analyticsInstance = HiAnalytics.getInstance(context)

    init {
        analyticsInstance.setAnalyticsEnabled(true)
    }

    override fun onEvent(type: String, bundle: Bundle) = analyticsInstance.onEvent(type, bundle)
}