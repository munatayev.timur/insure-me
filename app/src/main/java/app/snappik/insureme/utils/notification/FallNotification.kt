package app.snappik.insureme.utils.notification

import android.app.Service
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import app.snappik.insureme.R
import app.snappik.insureme.ui.main.MainActivity
import javax.inject.Inject

class FallNotification @Inject constructor(override val service: Service) : INotification {

    override val notificationId: Int = 1247

    override var channelId: String = service.getString(R.string.fall_notification_channel_id)

    override val channelName: String = service.getString(R.string.notification_channel_name)

    override val importance: Int = android.app.NotificationManager.IMPORTANCE_LOW

    override val enableSound: Boolean = false

    override val enableLights: Boolean = false

    override val enableVibrations: Boolean = false

    override val pendingIntentActivity: Class<*> = MainActivity::class.java

    override var pendingIntentBundle: Bundle = Bundle()

    override fun getNotification(): NotificationCompat.Builder = NotificationCompat
        .Builder(service, channelId)
        .setSmallIcon(R.mipmap.ic_launcher)
        .setVibrate(LongArray(1) { 0L })
        .setColor(ContextCompat.getColor(service, R.color.notification_background))
        .setColorized(true)
        .setAutoCancel(true)
        .setContentTitle("Fall detector listening")

    override fun showNotification() = service.startForeground(notificationId, getNotification().build())
}