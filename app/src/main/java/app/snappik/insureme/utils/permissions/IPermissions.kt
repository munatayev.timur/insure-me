package app.snappik.insureme.utils.permissions

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import app.snappik.insureme.R
import app.snappik.insureme.ui.dialog.InfoDialog

interface IPermissions {
    fun onActivityResult(requestCode: Int, resultCode: Int)
    fun onRequestPermissionsResult(requestCode: Int, grantResults: IntArray)
    fun checkPermissions(source: Callback, vararg newComePermission: String)

    interface Callback {
        fun onPermissionsGranted() = Unit
        fun onSettingsRequested() = Unit
        fun onPermissionRejected(activity: Activity){
            (activity as? AppCompatActivity)?.let {
                InfoDialog(activity.getString(R.string.guid_permissions_title),
                    activity.getString(R.string.guid_permissions)
                ).show(it.supportFragmentManager)
            }
        }
    }
}