package app.snappik.insureme.utils.billing

import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.R
import app.snappik.insureme.model.BillingProduct
import app.snappik.insureme.ui.dialog.InfoDialog

interface IBilling<TYPE, PURCHASE, PRODUCT> {

    val activity: FragmentActivity

    fun getProductInfo(result:(productInfo: BillingProduct) -> Unit)

    fun requestPurchase(product: BillingProduct)

    fun handlePurchase(purchase: PURCHASE?)

    fun consumePurchase(consumable: String)

    fun checkIsAlreadyOwn(result: (alreadySubscribed: Boolean) -> Unit)

    fun preProcess(success: () -> Unit)

    val productType: TYPE

    var success: () -> Unit

    fun onError(message: String?){
        InfoDialog(
            activity.getString(R.string.billing_general_title),
            "$message",
            onOk = {}
        ).show(activity.supportFragmentManager)
    }

    fun onError(res: Int) = onError(activity.getString(res))

    companion object {
        const val PRODUCT_ID = "app.insureme.subscription.month"
        const val REQ_CODE_BUY = 1452
    }
}