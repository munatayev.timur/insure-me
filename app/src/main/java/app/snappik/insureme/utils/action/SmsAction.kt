package app.snappik.insureme.utils.action

import android.telephony.SmsManager
import java.util.ArrayList

class SmsAction(private val destinationNumber: String,
                private val mText: String) : IAction {

    override suspend fun send() {
        try {
            val sms = SmsManager.getDefault()
            val parts: ArrayList<String?>? = sms.divideMessage("$APPENDIX_APP$mText")
            sms.sendMultipartTextMessage(destinationNumber, null, parts, null, null)
        }catch (e: Exception){}
    }

    companion object {
        private const val APPENDIX_APP = "[Insure Me App]"
    }
}