package app.snappik.insureme.utils.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat

interface INotification {

    val service: Service

    val notificationManager: NotificationManager
        get() = service.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    val notificationId: Int

    var channelId: String

    val channelName: String

    val importance: Int

    val enableSound: Boolean

    val enableLights: Boolean

    val enableVibrations: Boolean

    val pendingIntentActivity: Class<*>

    var pendingIntentBundle : Bundle

    fun getPendingIntent(): PendingIntent = Intent(service, pendingIntentActivity)
        .apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            putExtras(pendingIntentBundle)
        }.let {
            PendingIntent.getActivity(service, 0, it, PendingIntent.FLAG_UPDATE_CURRENT)
        }

    @RequiresApi(Build.VERSION_CODES.O)
    fun createNotificationChannel() = Companion.createNotificationChannel(
        notificationManager,
        channelId,
        channelName,
        importance,
        enableLights,
        enableVibrations,
        enableSound
    )

    fun getNotification(): NotificationCompat.Builder

    fun showNotification()

    companion object {
        @RequiresApi(Build.VERSION_CODES.O)
        fun createNotificationChannel(
            notificationManager: NotificationManager,
            channelId: String,
            channelName: String,
            importance: Int,
            enableLights: Boolean = false,
            enableVibrations: Boolean = false,
            enableSound: Boolean = false
        ) {
            NotificationChannel(channelId, channelName, importance)
                .apply {
                    enableLights(enableLights)
                    enableVibration(enableVibrations)
                    if (enableSound.not()) setSound(null, null)
                }.also {
                    notificationManager.createNotificationChannel(it)
                }
        }
    }
}