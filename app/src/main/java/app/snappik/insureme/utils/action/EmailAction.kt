package app.snappik.insureme.utils.action

import android.content.Context
import app.snappik.insureme.R
import java.util.Properties
import javax.mail.Authenticator
import javax.mail.Message
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

class EmailAction(
    private val text: String,
    email: String,
    context: Context,
    properties: Properties,
    nameSurname: String
): IAction {

    private val title = String.format(context.getString(R.string.email_from), nameSurname)

    private val props = Properties().apply {
        put("mail.smtp.auth", "true")
        put("mail.smtp.starttls.enable", "true")
        put("mail.smtp.host", properties.getProperty(SENDER_EMAIL_HOST))
        put("mail.smtp.port", properties.getProperty(SENDER_EMAIL_PORT))
    }

    private val authCredentials = PasswordAuthentication(properties.getProperty(SENDER_EMAIL), properties.getProperty(SENDER_EMAIL_PASSWORD))
    private val auth = object : Authenticator() { override fun getPasswordAuthentication() = authCredentials }
    private val mFrom = InternetAddress(properties.getProperty(SENDER_EMAIL), context.getString(R.string.app_name))
    private val recipient = InternetAddress.parse(email)

    override suspend fun send() {
        try {
            MimeMessage(Session.getInstance(props, auth))
                .apply {
                    setFrom(mFrom)
                    setRecipients(Message.RecipientType.TO, recipient)
                    subject = title
                    setText(text)
                }.also {
                    Transport.send(it)
                }
        } catch (e: Exception) {}
    }

    companion object {
        private const val SENDER_EMAIL = "insureme_email_login"
        private const val SENDER_EMAIL_PASSWORD = "insureme_email_password"
        private const val SENDER_EMAIL_HOST = "insureme_email_host"
        private const val SENDER_EMAIL_PORT = "insureme_email_port"
    }
}

