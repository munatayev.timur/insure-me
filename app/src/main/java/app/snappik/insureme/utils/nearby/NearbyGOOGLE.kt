package app.snappik.insureme.utils.nearby

import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.ui.dialog.InfoDialog
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.AdvertisingOptions
import com.google.android.gms.nearby.connection.ConnectionInfo
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback
import com.google.android.gms.nearby.connection.ConnectionResolution
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo
import com.google.android.gms.nearby.connection.DiscoveryOptions
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback
import com.google.android.gms.nearby.connection.Payload
import com.google.android.gms.nearby.connection.PayloadCallback
import com.google.android.gms.nearby.connection.PayloadTransferUpdate
import com.google.android.gms.nearby.connection.Strategy.P2P_POINT_TO_POINT
import javax.inject.Inject


class NearbyGOOGLE @Inject constructor(override val activity: FragmentActivity) : INearby {

    private val strategy = P2P_POINT_TO_POINT
    private val client = Nearby.getConnectionsClient(activity)
    private val serviceId = activity.packageName
    private var currentDeviceEntityName: String? = null
    private var currentDevicePushToken: String? = null

    override fun startAdvertising(
        currentDeviceName: String,
        currentDeviceToken: String?,
        onSuccess: (() -> Unit)?,
        onDevicePaired: (remoteDeviceName: String, remoteDeviceToken: String) -> Unit
    ) {
        val advertisingOptions: AdvertisingOptions = AdvertisingOptions.Builder().setStrategy(strategy).build()
        client.startAdvertising(currentDeviceName, serviceId, getCallback(false, onDevicePaired), advertisingOptions)
        .addOnFailureListener { it.showError("Can not start advertising") }
        .addOnSuccessListener {
            currentDeviceEntityName = currentDeviceName
            currentDevicePushToken = currentDeviceToken
            onSuccess?.invoke()
        }
    }

    override fun startDiscovery(
        onScannerStart: () -> Unit,
        onDeviceFound: (endpointId: String, name: String) -> Unit,
        onDeviceLost:(endpointId: String) -> Unit
    ) {
        val callback = object : EndpointDiscoveryCallback(){
            override fun onEndpointLost(endpointId: String) = onDeviceLost.invoke(endpointId)
            override fun onEndpointFound(endpointId: String, discoveryEndpointInfo : DiscoveredEndpointInfo) {
               if (discoveryEndpointInfo.serviceId == serviceId) onDeviceFound.invoke(endpointId, discoveryEndpointInfo.endpointName)
            }
        }
        client.startDiscovery(serviceId, callback, DiscoveryOptions.Builder().setStrategy(strategy).build())
         .addOnSuccessListener { onScannerStart.invoke() }
         .addOnFailureListener { it.showError("Can not start device scanner") }
    }

    override fun connect(entryId: String, onDevicePaired: (remoteDeviceName: String, remoteDeviceToken: String) -> Unit) {
        currentDeviceEntityName?.let { client.requestConnection(it, entryId, getCallback(true, onDevicePaired)) }
    }

    private fun getCallback(
        initiator: Boolean,
        result: (remoteDeviceName: String, remoteDeviceToken: String) -> Unit
    ) = object : ConnectionLifecycleCallback() {
        override fun onConnectionInitiated(endpointId: String, connectInfo: ConnectionInfo)  {
            if (initiator.not()) {
                val title = "Pair request"
                val message = "Do you want to pair with device ${connectInfo.endpointName}"
                InfoDialog(title, message, onOk = {
                    val callback = getDataCallback { result.invoke(connectInfo.endpointName, it) }
                    client.acceptConnection(endpointId, callback)
                }, onCancel = { client.rejectConnection(endpointId) }).show(activity.supportFragmentManager)
            } else client.acceptConnection(endpointId, getDataCallback {
                result.invoke(
                    connectInfo.endpointName,
                    it
                )
            })
        }

        override fun onConnectionResult(endpointId: String, result: ConnectionResolution) {
            when (result.status.statusCode) {
                ConnectionsStatusCodes.STATUS_OK -> sendCredentials(endpointId)
                ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED -> Exception("Connection rejected").showError("Disconnected")
                else -> Exception("Unknown connection status").showError("Connection was lost")
            }
        }
        override fun onDisconnected(endpointId: String) = Unit
    }

    override fun stopAdvertising() = client.stopAdvertising()

    override fun stopDiscovering() = client.stopDiscovery()

    override fun disconnectAll() = client.stopAllEndpoints()

    private fun sendCredentials(toEndpointId: String) = currentDevicePushToken?.let {
        val byteArray = it.toByteArray(Charsets.UTF_8)
        val bytesData: Payload = Payload.fromBytes(byteArray)
        client.sendPayload(toEndpointId, bytesData)
    } ?: Exception("There is no push token on current device").showError("Can not pair")

    private fun getDataCallback(result: ((data: String) -> Unit)? = null) = object : PayloadCallback() {
        override fun onPayloadTransferUpdate(endpointId: String, update: PayloadTransferUpdate) = Unit
        override fun onPayloadReceived(endpointId: String, payload: Payload) {
            result?.let {
                if (payload.type == Payload.Type.BYTES)
                    payload.asBytes()?.let { result.invoke(it.toString(Charsets.UTF_8)) }
            }
        }
    }
}
