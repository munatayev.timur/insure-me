package app.snappik.insureme.utils.view

import android.content.Context
import android.util.AttributeSet
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractLayoutDataBinding
import app.snappik.insureme.databinding.LayoutPinBinding
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.utils.Utilities.getMD5
import app.snappik.insureme.viewmodel.ActionViewModel
import app.snappik.insureme.viewmodel.SettingsViewModel
import app.snappik.insureme.BR

class PinLayout : AbstractLayoutDataBinding<LayoutPinBinding>, IPinEvent {

    var settings: Settings? = null

    var settingsViewModel: SettingsViewModel? = null

    var actionViewModel: ActionViewModel? = null

    override fun getViewId(): Int = R.layout.layout_pin

    constructor(context: Context) : super(context)

    constructor(context: Context, attr: AttributeSet) : super(context, attr)

    constructor(context: Context, attr: AttributeSet, i: Int) : super(context, attr, i)

    override fun onTextChanged(
        s: CharSequence,
        before: Int,
        after: Int,
        count: Int
    ) {
        settings?.let {
            if (count == it.pinLength) {
                actionViewModel?.tryUnlockPin(s.toString().getMD5(), it)
                BR.pinText.updateWith {
                    pinText = ""
                }
            }
        }
    }

    override fun add(text: String) {
        BR.pinText.updateWith {
            pinText = "${pinText ?: ""}${text}"
        }
    }

    override fun LayoutPinBinding.initVariables() {
        pinlayout = this@PinLayout
    }
}