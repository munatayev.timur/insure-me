package app.snappik.insureme.utils.map

import android.view.View

interface IMap {
    val mapView: View
    fun loadMap(lat: String, lon: String)
    fun onStart() = Unit
    fun onStop() = Unit
    fun onCreate() = Unit
    fun onResume() = Unit
    fun onPause() = Unit
    fun onLowMemory() = Unit
    fun onDestroy() = Unit

    companion object {
        const val MAP_BUNDLE_KEY = "MapViewBundleKey"
    }
}