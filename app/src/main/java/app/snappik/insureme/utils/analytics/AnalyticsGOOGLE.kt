package app.snappik.insureme.utils.analytics

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject

class AnalyticsGOOGLE @Inject constructor(context: Context) : IAnalytics {

    private val mFirebaseAnalytics = FirebaseAnalytics.getInstance(context)

    init {
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true)
    }

    override fun onEvent(type: String, bundle: Bundle) = mFirebaseAnalytics.logEvent(type, bundle)
}