package app.snappik.insureme.utils.action

import android.content.Context
import android.content.Intent
import android.net.Uri
import app.snappik.insureme.R
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.model.request.GooglePushRequest
import app.snappik.insureme.model.request.HuaweiPushRequest
import app.snappik.insureme.viewmodel.RetrofitViewModel

class PushAction(
    private val context: Context,
    private val text: String,
    private val lat: Double? = null,
    private val long: Double? = null,
    private val mUser: User?,
    private val token: String,
    private val retrofitViewModel: RetrofitViewModel,
    private val friend: Friend
) : IAction {

    private val host = context.getString(R.string.huawei_push_host)
    private val path = context.getString(R.string.huawei_push_path)
    private val mScheme = context.getString(R.string.huawei_push_scheme)
    private val title = context.getString(R.string.push_title)
    private val intent = Intent(Intent.ACTION_VIEW).apply {
        data = Uri.parse("$mScheme://$host$path?")
        lat?.let { putExtra(LATITUDE, it.toString()) }
        long?.let { putExtra(LONGITUDE, it.toString()) }
        putExtra(BODY, text)
    }

    override suspend fun send() {
        mUser?.let { user ->
            intent.putExtra(DISPLAY_NAME, user.displayName)
                .toUri(Intent.URI_INTENT_SCHEME)
                .also { uri ->
                    when (user.type) {
                        User.Type.HUAWEI -> retrofitViewModel.sendHuaweiPush(
                            context,
                            token,
                            HuaweiPushRequest.createFrom(title, text, friend.token, uri)
                        )
                        User.Type.GOOGLE -> retrofitViewModel.sendGooglePush(
                            context,
                            token,
                            GooglePushRequest.create(
                                friend.token,
                                title,
                                text,
                                lat?.toString(),
                                long?.toString()
                            )
                        )
                        User.Type.GUEST -> Unit
                    }
                }
        }
    }

    companion object {
        const val LATITUDE = "lat"
        const val LONGITUDE = "lon"
        const val BODY = "body"
        const val DISPLAY_NAME = "displayName"
        const val CHANNEL_ID = "channelId"
    }
}