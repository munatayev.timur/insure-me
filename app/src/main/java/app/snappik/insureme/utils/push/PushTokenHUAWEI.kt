package app.snappik.insureme.utils.push

import android.app.Application
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.utils.Utilities.getMD5
import com.huawei.agconnect.config.AGConnectServicesConfig
import com.huawei.hms.aaid.HmsInstanceId
import com.huawei.hms.push.HmsMessaging
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class PushTokenHUAWEI @Inject constructor(private val application: Application) : IPushToken {

    private val asyncScope = CoroutineScope(Dispatchers.IO)

    override fun initSubscription(user: User, success: (() -> Unit)?, failure: ((info: String) -> Unit)?) {
        asyncScope.launch {
            try {
                AGConnectServicesConfig
                    .fromContext(application)
                    .getString(APP_ID)
                    .also { appId ->
                        HmsInstanceId
                            .getInstance(application)
                            .getToken(appId, SERVICE_ID)
                            .also {
                                if (it.isNullOrEmpty().not()) subscribeToTopic(
                                    user,
                                    success,
                                    failure
                                )
                                else success?.invoke()
                            }
                    }
            } catch (e: Exception) {
                failure?.invoke(e.message ?: "Can not obtain token")
            }
        }
    }

    override fun subscribeToTopic(
        user: User,
        success: (() -> Unit)?,
        failure: ((info: String) -> Unit)?
    ) {
        try {
            HmsMessaging
                .getInstance(application)
                .subscribe(user.displayName.getMD5())
                .addOnSuccessListener { success?.invoke() }
                .addOnFailureListener { failure?.invoke(it.message ?: "Topic subscription done, but returned with error") }
        }catch (e: Exception){
            failure?.invoke(e.message ?: "Can not execute topic subscription")
        }
    }

    override fun unsubscribeToTopic(user: User) {
        asyncScope.launch {
            HmsMessaging
                .getInstance(application)
                .unsubscribe(user.displayName.getMD5())
        }
    }

    companion object {
        private const val SERVICE_ID = "HCM"
        private const val APP_ID = "client/app_id"
    }
}