package app.snappik.insureme.utils

import android.app.AlarmManager
import android.app.AlarmManager.RTC_WAKEUP
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import app.snappik.insureme.receiver.AlarmReceiver
import javax.inject.Inject

class AlarmHelper @Inject constructor(val context: Context) {

    private val am by lazy { context.getSystemService(Context.ALARM_SERVICE) as AlarmManager }

    fun removeAlarm(action: String, receiverClass: Class<*> = AlarmReceiver::class.java) {
        PendingIntent.getBroadcast(
            context,
            PENDING_ALARM_REQUEST_CODE,
            Intent(action).apply {
                setClass(context, receiverClass)
            },
            PendingIntent.FLAG_UPDATE_CURRENT
        ).also {
            am.cancel(it)
        }
    }

    fun setUpAlarm(time: Long, action: String, receiverClass: Class<*> = AlarmReceiver::class.java): PendingIntent = Intent(action)
        .apply { setClass(context, receiverClass) }
        .let { PendingIntent.getBroadcast(context, PENDING_ALARM_REQUEST_CODE, it, PendingIntent.FLAG_UPDATE_CURRENT)}
        .also { am.setExactAndAllowWhileIdle(RTC_WAKEUP, time, it) }

    companion object{
        private const val PENDING_ALARM_REQUEST_CODE = 0
    }
}