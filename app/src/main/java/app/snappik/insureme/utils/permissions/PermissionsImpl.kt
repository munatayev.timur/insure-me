package app.snappik.insureme.utils.permissions

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.PowerManager
import android.provider.Settings
import androidx.core.content.ContextCompat
import javax.inject.Inject

class PermissionsImpl @Inject constructor(private val activity: Activity) : IPermissions {

    private var iCallback: IPermissions.Callback? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int){
        when(requestCode) {
            BATTERY_OPTIMIZATION -> {
                if(resultCode == Activity.RESULT_OK){
                    iCallback?.onPermissionsGranted()
                } else iCallback?.onPermissionsGranted()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, grantResults: IntArray){
        when(requestCode) {
            MULTIPLE_PERMISSIONS -> {
                if (grantResults.find { it != PackageManager.PERMISSION_GRANTED } == null) {
                    if(isButterOptimized()) disableBatteryOptimization() else
                        iCallback?.onPermissionsGranted()
                } else iCallback?.onPermissionRejected(activity)
            }
        }
    }

    override fun checkPermissions(source: IPermissions.Callback, vararg newComePermission: String) {
        iCallback = source
        getNotGrantedPermissions(*newComePermission).run {
            when {
                this.isNotEmpty() -> activity.requestPermissions(this.toTypedArray(), MULTIPLE_PERMISSIONS)
                isButterOptimized() -> disableBatteryOptimization()
                else -> iCallback?.onPermissionsGranted()
            }
        }
    }

    private fun isButterOptimized(): Boolean{
        val pm = activity.getSystemService(Context.POWER_SERVICE) as PowerManager
        return pm.isIgnoringBatteryOptimizations(activity.packageName).not()
    }

    @SuppressLint("BatteryLife")
    private fun disableBatteryOptimization(){
        val intent = Intent().apply {
            action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
            data = Uri.parse("package:${activity.packageName}")
        }
        activity.startActivityForResult(intent, BATTERY_OPTIMIZATION)
    }

    private fun getNotGrantedPermissions(vararg newComePermission: String): List<String> = newComePermission
        .filter {
            val result = ContextCompat.checkSelfPermission(activity, it)
            result != PackageManager.PERMISSION_GRANTED
        }.toList()

    companion object {
        const val MULTIPLE_PERMISSIONS: Int = 15
        const val BATTERY_OPTIMIZATION: Int = 16
    }
}