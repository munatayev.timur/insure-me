package app.snappik.insureme.utils.map

import android.view.View
import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import javax.inject.Inject

class MapGoogle @Inject constructor(private val activity: FragmentActivity): IMap {

    override val mapView: MapView = MapView(activity).apply { id = View.generateViewId() }

    override fun loadMap(lat: String, lon: String) {
        mapView.getMapAsync {
            val locate = LatLng(lat.toDouble(), lon.toDouble())
            val markerString = activity.getString(R.string.location_last_known_location)
            val marker = MarkerOptions()
                .position(locate)
                .title(markerString)
            it.addMarker(marker)
            it.animateCamera(CameraUpdateFactory.newLatLngZoom(locate, 12.0f))
        }
    }

    override fun onCreate() {
        activity.supportFragmentManager
            .beginTransaction()
            .add(mapView.id, SupportMapFragment.newInstance(), SupportMapFragment::class.java.name)
            .commit()
    }
}