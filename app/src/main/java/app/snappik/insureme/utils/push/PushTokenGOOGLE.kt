package app.snappik.insureme.utils.push

import app.snappik.insureme.model.entity.User
import app.snappik.insureme.utils.Utilities.getMD5
import com.google.firebase.messaging.FirebaseMessaging

class PushTokenGOOGLE : IPushToken {

    override fun initSubscription(user: User, success: (() -> Unit)?, failure: ((info: String) -> Unit)?)
            = subscribeToTopic(user, success, failure)

    override fun subscribeToTopic(user: User, success: (() -> Unit)?, failure: ((info: String) -> Unit)?) {
        FirebaseMessaging
            .getInstance()
            .subscribeToTopic(user.displayName.getMD5())
            .addOnFailureListener { failure?.invoke(it.message ?: "Subscribe to topic filed") }
            .addOnCompleteListener {
                if (it.isSuccessful.not()) failure?.invoke("Subscribe successfully returned, but failed")
                else success?.invoke()
            }
    }

    override fun unsubscribeToTopic(user: User) {
        FirebaseMessaging
            .getInstance()
            .unsubscribeFromTopic(user.displayName.getMD5())
    }
}