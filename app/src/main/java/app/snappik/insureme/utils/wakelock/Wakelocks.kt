package app.snappik.insureme.utils.wakelock

interface Wakelocks {
    fun acquireServiceLock()
    fun releaseServiceLock()
}