package app.snappik.insureme.utils.view

import android.graphics.Color

interface IPinEvent {
    fun add(text: String)
    fun onTextChanged(s: CharSequence, before: Int, after: Int, count: Int)
    fun isShowControls() = false
    fun onRemoveClicked() = Unit
    fun getTextColor() = Color.WHITE
    fun onOkClicked(text: String?) = Unit
}