package app.snappik.insureme.utils.auth

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import app.snappik.insureme.InsureMeApplication
import app.snappik.insureme.model.entity.User
import com.huawei.hms.common.ApiException
import com.huawei.hms.support.hwid.HuaweiIdAuthManager
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParams
import com.huawei.hms.support.hwid.request.HuaweiIdAuthParamsHelper
import com.huawei.hms.support.hwid.service.HuaweiIdAuthService
import javax.inject.Inject

class AuthHUAWEI @Inject constructor(override val application: InsureMeApplication) : IAuth {

    override var status: MutableLiveData<IAuth.Status> = MutableLiveData(IAuth.Status.NOT_LOGGED_IN)

    override val responseCode: Int = 1002

    private val mAuthParam: HuaweiIdAuthParams by lazy {
        HuaweiIdAuthParamsHelper(HuaweiIdAuthParams.DEFAULT_AUTH_REQUEST_PARAM)
            .setIdToken()
            .setAccessToken()
            .createParams()
    }

    private val mAuthService: HuaweiIdAuthService by lazy { HuaweiIdAuthManager.getService(application, mAuthParam) }

    override fun getSignInIntent(): Intent = mAuthService.signInIntent

    override fun signOut(success: () -> Unit) {
        mAuthService.signOut()
            .addOnFailureListener { e -> e.showError("Sign out failed")}
            .addOnSuccessListener {
                status.postValue(IAuth.Status.NOT_LOGGED_IN)
                success.invoke()
            }
    }

    override fun onActivityResult(requestCode: Int, data: Intent?,
                                  success: (displayName: String, type: User.Type) -> Unit) {
        if(requestCode == responseCode) {
            HuaweiIdAuthManager.parseAuthResultFromIntent(data).run {
                if (isSuccessful) success.invoke(result.displayName, User.Type.HUAWEI)
                else Exception("Error code : ${(exception as ApiException).statusCode}").showError("Sign in failed")
            }
        }
    }

    override fun silentSignIn(success:(token: String) -> Unit) {
        mAuthService.silentSignIn()
            .addOnSuccessListener { success.invoke(it.accessToken) }
            .addOnFailureListener { success.invoke("") }
    }
}