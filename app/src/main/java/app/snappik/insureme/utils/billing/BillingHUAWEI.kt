package app.snappik.insureme.utils.billing

import android.content.Intent
import android.content.IntentSender.SendIntentException
import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.R
import app.snappik.insureme.hilt.UtilitiesModule.PROP_NAME
import app.snappik.insureme.model.BillingProduct
import app.snappik.insureme.utils.billing.IBilling.Companion.PRODUCT_ID
import app.snappik.insureme.utils.billing.IBilling.Companion.REQ_CODE_BUY
import com.huawei.hmf.tasks.OnSuccessListener
import com.huawei.hms.iap.Iap
import com.huawei.hms.iap.IapClient
import com.huawei.hms.iap.entity.InAppPurchaseData
import com.huawei.hms.iap.entity.OrderStatusCode
import com.huawei.hms.iap.entity.OwnedPurchasesReq
import com.huawei.hms.iap.entity.ProductInfo
import com.huawei.hms.iap.entity.ProductInfoReq
import com.huawei.hms.iap.entity.PurchaseIntentReq
import com.huawei.hms.iap.entity.PurchaseIntentResult
import com.huawei.hms.support.log.common.Base64
import java.nio.charset.StandardCharsets
import java.security.KeyFactory
import java.security.PublicKey
import java.security.Signature
import java.security.spec.X509EncodedKeySpec
import java.util.Properties


class BillingHUAWEI(override val activity: FragmentActivity) : IBilling<Int, Intent, ProductInfo> {

    private val iapClient = Iap.getIapClient(activity)

    private val properties = Properties().apply { load(activity.assets.open(PROP_NAME)) }

    private val productInfoReq = ProductInfoReq().apply {
        priceType = IapClient.PriceType.IN_APP_SUBSCRIPTION
        productIds = listOf(PRODUCT_ID)
    }

    override val productType: Int = IapClient.PriceType.IN_APP_SUBSCRIPTION

    override fun preProcess(success: () -> Unit) {
        iapClient.isEnvReady
                .addOnSuccessListener {
                    success.invoke()
                }
    }

    override fun getProductInfo(result: (productInfo: BillingProduct) -> Unit) {
        iapClient.obtainProductInfo(productInfoReq)
                .addOnSuccessListener { productInfoResult ->
                    if (productInfoResult != null && productInfoResult.productInfoList.isNotEmpty()) {
                        val productInfo = productInfoResult.productInfoList.first()
                        result.invoke(with(productInfo) {
                            BillingProduct(
                                    title = productName,
                                    description = productDesc,
                                    price = price,
                                    original = this
                            )
                        })
                    }
                }.addOnFailureListener {
                    onError(it.message)
                }
    }

    override fun requestPurchase(product: BillingProduct) {
        val req = PurchaseIntentReq().apply {
            productId = (product.original as ProductInfo).productId
            priceType = productType
            developerPayload = "Success"
        }

        iapClient.createPurchaseIntent(req)
                .addOnSuccessListener(object : OnSuccessListener<PurchaseIntentResult?> {
                    override fun onSuccess(result: PurchaseIntentResult?) {
                        if (result == null || result.status == null) {
                            onError(R.string.billing_null_purchase_request)
                            return
                        }

                        if (result.status.hasResolution() && result.returnCode == 0) {
                            try {
                                result.status.startResolutionForResult(activity, REQ_CODE_BUY)
                            } catch (exp: SendIntentException) {
                                onError(exp.message)
                            }
                        } else onError(R.string.billing_no_resolution)
                    }
                }).addOnFailureListener { e ->
                    onError(e.message)
                }
    }

    override fun handlePurchase(purchase: Intent?) {
        if (purchase == null) {
            onError(R.string.billing_empty_handle)
            return
        }

        val purchaseResultInfo = iapClient.parsePurchaseResultInfoFromIntent(purchase)
        when (purchaseResultInfo.returnCode) {
            OrderStatusCode.ORDER_STATE_SUCCESS -> {

                val isSuccessfullySubscribed: Boolean = doCheck(
                        purchaseResultInfo.inAppPurchaseData,
                        purchaseResultInfo.inAppDataSignature
                )

                if (isSuccessfullySubscribed) success.invoke()
                else onError(R.string.billing_incorrect_purchase_sign)
            }
            OrderStatusCode.ORDER_STATE_CANCEL -> Unit
            OrderStatusCode.ORDER_PRODUCT_OWNED -> onError(R.string.billing_already_own)
            else -> onError(R.string.billing_state_unknown)
        }
    }

    override fun consumePurchase(consumable: String) = Unit

    override fun checkIsAlreadyOwn(result: (alreadySubscribed: Boolean) -> Unit) {
        val req = OwnedPurchasesReq().apply {
            priceType = productType
        }

        iapClient.obtainOwnedPurchases(req)
                .addOnSuccessListener {
                    if (it != null && it.inAppPurchaseDataList != null) {
                        if (it.inAppPurchaseDataList.isEmpty()) {
                            result.invoke(false)
                            return@addOnSuccessListener
                        }

                        val json = it.inAppPurchaseDataList.first()
                        val purchaseData = InAppPurchaseData(json)

                        val isSuccessfullySubscribed: Boolean = doCheck(
                                it.inAppPurchaseDataList.first(),
                                it.inAppSignature.first()
                        )

                        if (isSuccessfullySubscribed) {
                            val isAlreadySubscribed = purchaseData.productId == PRODUCT_ID && purchaseData.isSubValid
                            result.invoke(isAlreadySubscribed)
                        } else onError(R.string.billing_subscription_validation)

                    } else onError(R.string.billing_already_subscribed_error)
                }.addOnFailureListener {
                    onError(it.message)
                }
    }

    private fun doCheck(content: String, sign: String?): Boolean {
        if (sign == null) return false
        try {
            val keyFactory: KeyFactory = KeyFactory.getInstance("RSA")
            val encodedKey: ByteArray = Base64.decode(properties.getProperty(HUAWEI_PAYMENT_PUBLIC_KEY))
            val pubKey: PublicKey = keyFactory.generatePublic(X509EncodedKeySpec(encodedKey))
            val signature = Signature.getInstance("SHA256WithRSA")
            signature.initVerify(pubKey)
            signature.update(content.toByteArray(StandardCharsets.UTF_8))
            val bSign: ByteArray = Base64.decode(sign)
            return signature.verify(bSign)
        } catch (e: Exception) {
            throw e
        }
    }

    override var success: () -> Unit = { onError("Billing working incorrectly") }

    companion object {
        private const val HUAWEI_PAYMENT_PUBLIC_KEY = "insureme_huawei_public_key"
    }
}