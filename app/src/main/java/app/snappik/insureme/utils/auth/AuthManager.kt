package app.snappik.insureme.utils.auth

import android.content.Context
import android.content.Intent
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.viewmodel.UserViewModel
import javax.inject.Inject

class AuthManager @Inject constructor(val huawei: IAuth,
                                      val google: IAuth,
                                      private val userViewModel: UserViewModel) {

    fun onActivityResult(requestCode: Int, data: Intent?){
        huawei.onActivityResult(requestCode, data, success)
        google.onActivityResult(requestCode, data, success)
    }

    fun getAuth(type: User.Type? = null): IAuth? = when (type ?: userViewModel.user?.value?.type ?: User.Type.GUEST) {
        User.Type.GOOGLE -> google
        User.Type.HUAWEI -> huawei
        else -> null
    }

    fun signInAsGuest(context: Context) = userViewModel.signInAsGuest(context)

    private val success = fun(displayName: String, type: User.Type){
        userViewModel.signIn(displayName, type)
    }
}