package app.snappik.insureme.utils.view

import android.content.Context
import android.util.AttributeSet
import android.widget.ScrollView

class CustomScrollView : ScrollView {
    var onBottomReachedListener: OnBottomReachedListener? = null

    constructor(
        context: Context?, attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle)

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context,
        attrs
    )

    constructor(context: Context?) : super(context)

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        val view = getChildAt(childCount - 1)
        val diff = view.bottom - (height + scrollY) - view.paddingBottom
        if (diff <= 0 && onBottomReachedListener != null) onBottomReachedListener!!.onBottomReached()
        super.onScrollChanged(l, t, oldl, oldt)
    }

    interface OnBottomReachedListener {
        fun onBottomReached()
    }
}