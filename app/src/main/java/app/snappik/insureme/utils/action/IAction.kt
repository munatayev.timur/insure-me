package app.snappik.insureme.utils.action

interface IAction {
    suspend fun send()
}