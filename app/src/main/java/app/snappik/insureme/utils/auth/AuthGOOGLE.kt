package app.snappik.insureme.utils.auth

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import app.snappik.insureme.InsureMeApplication
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.viewmodel.RetrofitViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import javax.inject.Inject


class AuthGOOGLE @Inject constructor(
    override val application: InsureMeApplication,
    private val retrofitViewModel: RetrofitViewModel
) : IAuth {

    override var status: MutableLiveData<IAuth.Status> = MutableLiveData(IAuth.Status.NOT_LOGGED_IN)

    override val responseCode: Int = 1003

    private val mAuthParams = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()

    private val mAuthService: GoogleSignInClient by lazy {
        GoogleSignIn.getClient(
            application,
            mAuthParams
        )
    }

    override fun getSignInIntent(): Intent = mAuthService.signInIntent

    override fun signOut(success: () -> Unit) {
        mAuthService.signOut()
            .addOnFailureListener { e -> e.showError("Sign out failed") }
            .addOnSuccessListener {
                status.postValue(IAuth.Status.NOT_LOGGED_IN)
                success.invoke()
            }
    }

    override fun onActivityResult(
        requestCode: Int, data: Intent?,
        success: (displayName: String, type: User.Type) -> Unit
    ) {
        if (requestCode == responseCode) {
            GoogleSignIn.getSignedInAccountFromIntent(data).let { task ->
                try {
                    task.getResult(ApiException::class.java)?.let { account ->
                        account.displayName?.let {
                            success.invoke(it, User.Type.GOOGLE)
                        } ?: Exception("No account data").showError("Sign in failed")
                    } ?: Exception("No account data").showError("Sign in failed")
                } catch (e: ApiException) {
                    e.showError("Sign in failed")
                }
            }
        }
    }

    override fun silentSignIn(success: (token: String) -> Unit) {
        retrofitViewModel.getAccessTokenGoogle(application) {
            success.invoke(it ?:"")
        }
    }
}