package app.snappik.insureme.utils.notification

import android.app.Service
import android.os.Bundle
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import app.snappik.insureme.R
import app.snappik.insureme.ui.main.MainActivity
import app.snappik.insureme.utils.Utilities.timeToString
import javax.inject.Inject

class TimerNotification @Inject constructor(override val service: Service) : INotification {

    private val notificationLayout: RemoteViews = RemoteViews(service.packageName, R.layout.notification_background)

    override val notificationId: Int = 1248

    override var channelId: String = service.getString(R.string.default_notification_channel_id)

    override val channelName: String = service.getString(R.string.notification_channel_name)

    override val importance: Int = android.app.NotificationManager.IMPORTANCE_LOW

    override val enableSound: Boolean = false

    override val enableLights: Boolean = false

    override val enableVibrations: Boolean= false

    override val pendingIntentActivity: Class<*> = MainActivity::class.java

    override var pendingIntentBundle: Bundle = Bundle()
    val listener = object : IEvent {
        override fun onTimeUpdated(time: Long) {
            notificationLayout.setTextViewText(R.id.notification_text, service.timeToString(time))
            notificationManager.notify(notificationId, getNotification().build())
        }

        override fun onActionRequired(isAction: Boolean) {
            val notTitle = if (isAction) R.string.notification_title_action_true else R.string.notification_title_action_false
            val colorText = ContextCompat.getColor(service, R.color.notification_text)
            val colorRequired = ContextCompat.getColor(service, R.color.red)
            notificationLayout.setTextColor(R.id.notification_text, if (isAction) colorRequired else colorText)
            notificationLayout.setTextViewText(R.id.notification_title, service.getString(notTitle))
            notificationManager.notify(notificationId, getNotification().build())
        }
    }

    override fun getNotification(): NotificationCompat.Builder = NotificationCompat
        .Builder(service, channelId)
        .setSmallIcon(R.mipmap.ic_launcher)
        .setContentIntent(getPendingIntent())
        .setVibrate(LongArray(1) { 0L })
        .setStyle(NotificationCompat.DecoratedCustomViewStyle())
        .setCustomContentView(notificationLayout)
        .setColor(ContextCompat.getColor(service, R.color.notification_background))
        .setColorized(true)

    override fun showNotification() = service.startForeground(notificationId, getNotification().build())

    interface IEvent {
        fun onTimeUpdated(time: Long)
        fun onActionRequired(isAction: Boolean)
    }
}