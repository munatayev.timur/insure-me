package app.snappik.insureme.utils.notification

import android.app.Service
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import app.snappik.insureme.R
import app.snappik.insureme.ui.map.MapActivity
import javax.inject.Inject

class MessageNotification @Inject constructor(override val service: Service) : INotification {

    override val notificationId: Int = 1249

    override var channelId: String = service.getString(R.string.push_notification_channel_id)

    override val channelName: String = service.getString(R.string.push_notification_channel_name)

    override val importance: Int = android.app.NotificationManager.IMPORTANCE_HIGH

    override val enableSound: Boolean = true

    override val enableLights: Boolean = true

    override val enableVibrations: Boolean = true

    override val pendingIntentActivity: Class<*> = MapActivity::class.java

    override var pendingIntentBundle: Bundle = Bundle()

    private var title: String = ""

    private var body: String = ""

    override fun getNotification(): NotificationCompat.Builder = NotificationCompat
        .Builder(service, channelId)
        .setSmallIcon(R.mipmap.ic_launcher)
        .setContentIntent(getPendingIntent())
        .setVibrate(LongArray(1) { 0L })
        .setColor(ContextCompat.getColor(service, R.color.notification_background))
        .setStyle(NotificationCompat.BigTextStyle().bigText(body))
        .setColorized(true)
        .setAutoCancel(true)
        .setContentTitle(title)
        .setContentText(body)

    fun setText(title: String, body: String){
        this.title = title
        this.body = body
    }

    override fun showNotification() = notificationManager.notify(notificationId, getNotification().build())
}