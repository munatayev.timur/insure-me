package app.snappik.insureme.utils.push

import app.snappik.insureme.model.entity.User
import javax.inject.Inject

class PushTokenManager @Inject constructor(private val huawei: IPushToken, private val  google: IPushToken) {
    fun getPushService(user: User) : IPushToken = if(user.type == User.Type.GOOGLE) google else huawei
}