package app.snappik.insureme.utils.biometric

import androidx.fragment.app.Fragment
import java.util.concurrent.Executor

interface IBiometric {
    var onSuccess: () -> Unit
    fun auth(fragment: Fragment, executor: Executor)
    fun canAuthWithFingerprint(): Boolean
}