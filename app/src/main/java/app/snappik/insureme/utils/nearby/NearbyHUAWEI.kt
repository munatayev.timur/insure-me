package app.snappik.insureme.utils.nearby

import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.ui.dialog.InfoDialog
import com.huawei.agconnect.config.AGConnectServicesConfig
import com.huawei.hms.nearby.Nearby
import com.huawei.hms.nearby.StatusCode
import com.huawei.hms.nearby.discovery.BroadcastOption
import com.huawei.hms.nearby.discovery.ConnectCallback
import com.huawei.hms.nearby.discovery.ConnectInfo
import com.huawei.hms.nearby.discovery.ConnectResult
import com.huawei.hms.nearby.discovery.Policy
import com.huawei.hms.nearby.discovery.ScanEndpointCallback
import com.huawei.hms.nearby.discovery.ScanEndpointInfo
import com.huawei.hms.nearby.discovery.ScanOption
import com.huawei.hms.nearby.transfer.Data
import com.huawei.hms.nearby.transfer.DataCallback
import com.huawei.hms.nearby.transfer.TransferStateUpdate
import javax.inject.Inject

class NearbyHUAWEI @Inject constructor(override val activity: FragmentActivity) : INearby {

    private val broadcastPolicy = BroadcastOption.Builder().setPolicy(Policy.POLICY_STAR).build()
    private val startPolicy = ScanOption.Builder().setPolicy(Policy.POLICY_STAR).build()
    private val appId by lazy { AGConnectServicesConfig.fromContext(activity).getString("client/app_id") }
    private val engine = Nearby.getDiscoveryEngine(activity)
    private var currentDeviceEntityName: String? = null
    private var currentDevicePushToken: String? = null

    override fun startAdvertising(currentDeviceName: String,
                                  currentDeviceToken: String?,
                                  onSuccess: (() -> Unit)?,
                                  onDevicePaired: (remoteDeviceName: String, remoteDeviceToken: String) -> Unit) {
        engine.startBroadcasting(currentDeviceName, appId, getCallback(false, onDevicePaired), broadcastPolicy)
        .addOnFailureListener { it.showError("Can not start advertising") }
        .addOnSuccessListener {
            currentDeviceEntityName = currentDeviceName
            currentDevicePushToken = currentDeviceToken
            onSuccess?.invoke()
        }
    }

    override fun startDiscovery(onScannerStart: () -> Unit,
                                onDeviceFound: (endpointId: String, name: String) -> Unit,
                                onDeviceLost:(endpointId: String) -> Unit
    ) {
        val connectionCallback = object : ScanEndpointCallback() {
            override fun onLost(endpointId: String) = onDeviceLost.invoke(endpointId)
            override fun onFound(endpointId: String, discoveryEndpointInfo: ScanEndpointInfo) {
                if (discoveryEndpointInfo.serviceId == appId) onDeviceFound.invoke(endpointId, discoveryEndpointInfo.name)
            }
        }
        engine.startScan(appId, connectionCallback, startPolicy)
                .addOnSuccessListener { onScannerStart.invoke() }
                .addOnFailureListener { it.showError("Can not start device scanner") }
    }

    override fun connect(entryId: String, onDevicePaired: (remoteDeviceName: String, remoteDeviceToken: String) -> Unit) {
        engine.requestConnect(currentDeviceEntityName, entryId, getCallback(true, onDevicePaired)).addOnFailureListener { it.showError("Can connect to device") }
    }

    override fun stopAdvertising() = engine.stopBroadcasting()

    override fun stopDiscovering() = engine.stopScan()

    override fun disconnectAll() = engine.disconnectAll()

    private fun getCallback(initiator: Boolean, result: (remoteDeviceName: String, remoteDeviceToken: String) -> Unit) = object : ConnectCallback() {
        override fun onEstablish(endpointId: String, connectInfo: ConnectInfo) {
            if (initiator.not()) {
                val title = "Pair request"
                val message = "Do you want to pair with device ${connectInfo.endpointName}"
                InfoDialog(title, message, onOk = {
                    val callback = getDataCallback { result.invoke(connectInfo.endpointName, it) }
                    engine.acceptConnect(endpointId, callback)
                }, onCancel = { engine.rejectConnect(endpointId) }).show(activity.supportFragmentManager)
            } else engine.acceptConnect(endpointId, getDataCallback { result.invoke(connectInfo.endpointName, it) })
        }

        override fun onResult(endpointId: String, result: ConnectResult) {
            when (result.status.statusCode) {
                StatusCode.STATUS_SUCCESS -> sendCredentials(endpointId)
                StatusCode.STATUS_CONNECT_REJECTED -> Exception("Connection rejected").showError("Disconnected")
                else -> Exception("Unknown connection status").showError("Connection was lost")
            }
        }
        override fun onDisconnected(endpointId: String) = Unit
    }

    private fun sendCredentials(toEndpointId: String) = currentDevicePushToken?.let {
        val byteArray = it.toByteArray(Charsets.UTF_8)
        val bytesData: Data = Data.fromBytes(byteArray)
        Nearby.getTransferEngine(activity).sendData(toEndpointId, bytesData)
    } ?: Exception("There is no push token on current device").showError("Can not pair")

    private fun getDataCallback(result: ((data: String) -> Unit)? = null) = object : DataCallback() {
        override fun onTransferUpdate(endpointId: String, update: TransferStateUpdate) = Unit
        override fun onReceived(endpointId: String, data: Data) {
            result?.let {
                if (data.type == Data.Type.BYTES) {
                    val receivedBytes = data.asBytes()
                    result.invoke(receivedBytes.toString(Charsets.UTF_8))
                }
            }
        }
    }
}