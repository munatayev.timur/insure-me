package app.snappik.insureme.utils.auth

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import app.snappik.insureme.InsureMeApplication
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.ui.dialog.InfoDialog

interface IAuth {

    var status: MutableLiveData<Status>
    val application: InsureMeApplication
    val responseCode: Int
    fun signOut(success: () -> Unit)
    fun onActivityResult(requestCode: Int, data: Intent?, success: (displayName: String, type: User.Type) -> Unit)
    fun silentSignIn(success:(token: String) -> Unit)
    fun getSignInIntent(): Intent

    fun signIn(){
        status.postValue(Status.PROGRESS)
        application.currentActivity?.startActivityForResult(getSignInIntent(), responseCode)
    }

    fun Exception.showError(title: String) {
        status.postValue(Status.NOT_LOGGED_IN)
        application.currentActivity?.let {
            InfoDialog(title, message ?: "Not details").show(it.supportFragmentManager)
        }
    }

    enum class Status { NOT_LOGGED_IN, PROGRESS }
}