package app.snappik.insureme.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import app.snappik.insureme.R
import app.snappik.insureme.model.Strategy
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.utils.action.EmailAction
import app.snappik.insureme.utils.action.IAction
import app.snappik.insureme.utils.action.PushAction
import app.snappik.insureme.utils.action.SmsAction
import app.snappik.insureme.viewmodel.RetrofitViewModel
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch
import java.util.ArrayList
import java.util.Properties
import javax.inject.Inject


class ActionHelper @Inject constructor(private val context: Context,
                                       private val properties: Properties,
                                       private val retrofitViewModel: RetrofitViewModel) {


    var user: User? = null
    var friends: List<Friend>? = null
    var token: String = ""

    fun doAction(settings: Settings, finished: () -> Unit) {
        val text = "\n${context.getString(R.string.sms_message_default)}\n${settings.message}\n"
        if (settings.sendLocation) {
            getLastKnownLocation(context) {
                val mText = if(it?.latitude != null)
                    text.plus("${context.getString(R.string.sms_last_known_location)} ${it.latitude} ${it.longitude}") else text
                doAction(settings, mText, finished, it?.latitude, it?.longitude)
            }
        } else doAction(settings, text, finished)
    }

    private fun doAction(settings: Settings,
                     mText: String,
                     finished: () -> Unit,
                     lat: Double? = null,
                     long: Double? = null) = CoroutineScope(Dispatchers.Main).launch {

        val actionList = ArrayList<IAction>()

        if(settings.strategies.contains(Strategy.SMS)) {
            val smsAction = SmsAction(settings.destinationNumber, mText)
            actionList.add(smsAction)
        }
        if(settings.strategies.contains(Strategy.EMAIL)){
            val emailAction = EmailAction(mText, settings.email, context, properties, settings.nameSurname)
            actionList.add(emailAction)
        }
        if(settings.strategies.contains(Strategy.PUSH) && token.isNotEmpty()){
            friends?.forEach {
                if(it.selected) {
                    val pushAction = PushAction(context, mText, lat, long, user, token, retrofitViewModel, it)
                    actionList.add(pushAction)
                }
            }
        }
        actionList.map {
            async(Dispatchers.IO){ it.send() }
        }.awaitAll()
        finished.invoke()
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation(context: Context, result: (location: Location?) -> Unit) {
        LocationServices.getFusedLocationProviderClient(context).lastLocation
                .addOnSuccessListener { location: Location? -> result.invoke(location) }
    }
}