package app.snappik.insureme.utils.billing

import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.R
import app.snappik.insureme.model.BillingProduct
import app.snappik.insureme.utils.billing.IBilling.Companion.PRODUCT_ID
import com.android.billingclient.api.AcknowledgePurchaseParams
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchasesUpdatedListener
import com.android.billingclient.api.SkuDetails
import com.android.billingclient.api.SkuDetailsParams

class BillingGoogle(override val activity: FragmentActivity) : IBilling<String, Purchase, SkuDetails> {

    override val productType: String = BillingClient.SkuType.SUBS

    private val purchaseUpdateListener = PurchasesUpdatedListener { billingResult, purchases ->
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
            purchases?.forEach { handlePurchase(it) }
        }
    }

    private val billingClient = BillingClient.newBuilder(activity)
            .setListener(purchaseUpdateListener)
            .enablePendingPurchases()
            .build()

    override fun preProcess(success: () -> Unit) {
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingServiceDisconnected() = Unit
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    success.invoke()
                }
            }
        })
    }

    override fun getProductInfo(result: (productInfo: BillingProduct) -> Unit) {
        val params = SkuDetailsParams
                .newBuilder()
                .setSkusList(arrayListOf(PRODUCT_ID))
                .setType(productType)

        billingClient.querySkuDetailsAsync(params.build()) { billingResp, list ->
            val firstProduct = list?.firstOrNull()
            if (billingResp.responseCode == BillingClient.BillingResponseCode.OK && firstProduct != null) {
                result.invoke(with(firstProduct) {
                    BillingProduct(
                            title = title,
                            description = description,
                            price = price,
                            original = this
                    )
                })
            } else onError(billingResp.debugMessage)
        }
    }

    override fun requestPurchase(product: BillingProduct) {
        val flowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(product.original as SkuDetails)
                .build()
        val responseCode = billingClient.launchBillingFlow(activity, flowParams).responseCode
        if (responseCode != BillingClient.BillingResponseCode.OK) {
            onError(String.format(activity.getString(R.string.billing_failed_with_code), responseCode.toString()))
        }
    }

    override fun handlePurchase(purchase: Purchase?) {
        purchase?.let {
            if (it.purchaseState == Purchase.PurchaseState.PURCHASED) {
                if (!it.isAcknowledged) {
                    consumePurchase(it.purchaseToken)
                } else success.invoke()
            } else if (it.purchaseState == Purchase.PurchaseState.PENDING) onError(R.string.billing_state_pending)
            else onError(R.string.billing_state_unknown)
        } ?: onError(R.string.billing_empty_handle)
    }

    override fun consumePurchase(consumable: String) {
        val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                .setPurchaseToken(consumable)
        billingClient.acknowledgePurchase(acknowledgePurchaseParams.build()) {
            if (it.responseCode == BillingClient.BillingResponseCode.OK) success.invoke()
            else onError(R.string.billing_google_not_acknowledged_purchase)
        }
    }

    override fun checkIsAlreadyOwn(result: (alreadySubscribed: Boolean) -> Unit) {
        val queryResult = billingClient.queryPurchases(BillingClient.SkuType.SUBS)
        val isAlreadySubscribed = queryResult.purchasesList.isNullOrEmpty().not() && queryResult.purchasesList?.find { it.sku == PRODUCT_ID } != null
        result.invoke(isAlreadySubscribed)
    }

    override var success: () -> Unit = { onError("Billing working incorrectly") }
}