package app.snappik.insureme.hilt

import android.app.Application
import app.snappik.insureme.utils.analytics.AnalyticsGOOGLE
import app.snappik.insureme.utils.analytics.AnalyticsHUAWEI
import app.snappik.insureme.utils.analytics.IAnalytics
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AnalyticsModule {

    @Singleton
    @Provides
    fun provideMap(app: Application): IAnalytics = if (isGooglePlayMarket(app)) AnalyticsGOOGLE(app) else AnalyticsHUAWEI(app)

    private fun isGooglePlayMarket(app: Application): Boolean{
        val applicationInfo = app.packageManager.getApplicationInfo(app.packageName, 0)
        val installPackage = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            app.packageManager.getInstallSourceInfo(applicationInfo.packageName).installingPackageName
        } else app.packageManager.getInstallerPackageName(applicationInfo.packageName)
        return if(installPackage.isNullOrEmpty()) true else installPackage == "com.android.vending"
    }
}