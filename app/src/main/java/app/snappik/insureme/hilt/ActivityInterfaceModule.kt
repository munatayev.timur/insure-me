package app.snappik.insureme.hilt

import app.snappik.insureme.utils.permissions.IPermissions
import app.snappik.insureme.utils.permissions.PermissionsImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent


@Module
@InstallIn(ActivityComponent::class)
abstract class ActivityInterfaceModule {

    @Binds
    abstract fun bindPermissions(impl: PermissionsImpl): IPermissions
}