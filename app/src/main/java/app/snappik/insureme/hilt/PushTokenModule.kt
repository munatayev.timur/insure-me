package app.snappik.insureme.hilt

import android.app.Application
import app.snappik.insureme.utils.push.IPushToken
import app.snappik.insureme.utils.push.PushTokenGOOGLE
import app.snappik.insureme.utils.push.PushTokenHUAWEI
import app.snappik.insureme.utils.push.PushTokenManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import javax.inject.Singleton


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PushTokenGoogle

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class PushTokenHuawei

@Module
@InstallIn(SingletonComponent::class)
object PushTokenModule {

    @Provides
    @PushTokenGoogle
    fun bindPushTokenGoogle(): IPushToken = PushTokenGOOGLE()

    @Provides
    @PushTokenHuawei
    fun bindPushTokenHuawei(application: Application): IPushToken = PushTokenHUAWEI(application)

    @Singleton
    @Provides
    fun bindPushTokenManager(@PushTokenGoogle google: IPushToken,
                             @PushTokenHuawei huawei: IPushToken): PushTokenManager = PushTokenManager(huawei, google)
}