package app.snappik.insureme.hilt

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory
import app.snappik.insureme.database.repository.FriendRepository
import app.snappik.insureme.database.repository.SettingsRepository
import app.snappik.insureme.database.repository.UserRepository
import app.snappik.insureme.retrofit.RetrofitRepository
import app.snappik.insureme.viewmodel.FriendViewModel
import app.snappik.insureme.viewmodel.RetrofitViewModel
import app.snappik.insureme.viewmodel.SettingsViewModel
import app.snappik.insureme.viewmodel.ActionViewModel
import app.snappik.insureme.viewmodel.UserViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object VMModel {

    @Singleton
    @Provides
    fun provideUserViewModel(app: Application, userRepository: UserRepository): UserViewModel =
            app.create(UserViewModel::class.java).apply { this.userRepository = userRepository }

    @Singleton
    @Provides
    fun provideFriendViewModel(app: Application, friendRepository: FriendRepository): FriendViewModel =
            app.create(FriendViewModel::class.java).apply { this.friendRepository = friendRepository }

    @Singleton
    @Provides
    fun provideRetrofitViewModel(app: Application, retrofitRepository: RetrofitRepository): RetrofitViewModel =
            app.create(RetrofitViewModel::class.java).apply { this.retrofitRepository = retrofitRepository }

    @Singleton
    @Provides
    fun provideSettingsViewModel(app: Application, settingsRepository: SettingsRepository): SettingsViewModel =
            app.create(SettingsViewModel::class.java).apply { this.settingsRepository = settingsRepository }

    @Singleton
    @Provides
    fun provideTimerViewModel(app: Application) = app.create(ActionViewModel::class.java)

    private fun <T : ViewModel> Application.create(kk: Class<T>) = AndroidViewModelFactory.getInstance(this).create(kk)
}