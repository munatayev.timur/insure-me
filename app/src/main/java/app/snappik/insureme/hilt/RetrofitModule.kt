package app.snappik.insureme.hilt

import app.snappik.insureme.retrofit.ApiService
import app.snappik.insureme.retrofit.ApiUtils
import app.snappik.insureme.retrofit.RetrofitRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {

    @Singleton
    @Provides
    fun provideApiService() = ApiUtils.apiService()

    @Singleton
    @Provides
    fun provideRetrofitRepository(apiService: ApiService) = RetrofitRepository(apiService)
}