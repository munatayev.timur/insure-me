package app.snappik.insureme.hilt

import app.snappik.insureme.utils.nearby.INearby
import app.snappik.insureme.utils.nearby.NearbyGOOGLE
import app.snappik.insureme.utils.nearby.NearbyHUAWEI
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class NearbyGoogle

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class NearbyHuawei

@Module
@InstallIn(ActivityComponent::class)
abstract class NearbyModule {

    @Binds
    @NearbyHuawei
    abstract fun bindHuaweiNearBy(impl: NearbyHUAWEI): INearby

    @Binds
    @NearbyGoogle
    abstract fun bindGoogleNearBy(impl: NearbyGOOGLE): INearby
}