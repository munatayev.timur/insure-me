package app.snappik.insureme.hilt

import app.snappik.insureme.utils.notification.FallNotification
import app.snappik.insureme.utils.notification.INotification
import app.snappik.insureme.utils.notification.MessageNotification
import app.snappik.insureme.utils.notification.TimerNotification
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ServiceComponent
import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class TimerNotify

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class MessageNotify

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class FallNotify

@Module
@InstallIn(ServiceComponent::class)
abstract class NotificationModule {

    @Binds
    @FallNotify
    abstract fun bindFallNotification(impl: FallNotification) : INotification

    @Binds
    @TimerNotify
    abstract fun bindTimerNotification(impl: TimerNotification) : INotification

    @Binds
    @MessageNotify
    abstract fun bindMessageNotification(impl: MessageNotification) : INotification
}