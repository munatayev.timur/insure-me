package app.snappik.insureme.hilt

import android.app.Application
import android.content.Context
import app.snappik.insureme.utils.ActionHelper
import app.snappik.insureme.utils.AlarmHelper
import app.snappik.insureme.utils.wakelock.WakeLockManager
import app.snappik.insureme.viewmodel.RetrofitViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import java.util.Properties
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UtilitiesModule {

    @Singleton
    @Provides
    fun provideWakeLock(app: Application) = WakeLockManager(app)

    @Singleton
    @Provides
    fun provideActionHelper(app: Application,
                            properties: Properties,
                            retrofitViewModel: RetrofitViewModel) =
        ActionHelper(app, properties, retrofitViewModel)

    @Singleton
    @Provides
    fun provideAlarmHelper(@ApplicationContext appContext: Context) = AlarmHelper(appContext)

    @Singleton
    @Provides
    fun provideProperties(app: Application) = Properties().apply {
        load(app.assets.open(PROP_NAME))
    }

    const val PROP_NAME = "app.properties"
}