package app.snappik.insureme.hilt

import android.os.Build
import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.utils.biometric.BiometricGoogle
import app.snappik.insureme.utils.biometric.BiometricHuawei
import app.snappik.insureme.utils.biometric.IBiometric
import app.snappik.insureme.utils.map.IMap
import app.snappik.insureme.utils.map.MapGoogle
import app.snappik.insureme.utils.map.MapHUAWEI
import app.snappik.insureme.utils.nearby.INearby
import app.snappik.insureme.utils.nearby.NearbyManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import java.util.Locale

@Module
@InstallIn(ActivityComponent::class)
class ActivityClassModule {

    @Provides
    fun provideMap(fragmentActivity: FragmentActivity): IMap {
        return if (Build.MANUFACTURER.toLowerCase(Locale.ROOT).contains(HUAWEI_BRAND)) MapHUAWEI(fragmentActivity)
        else MapGoogle(fragmentActivity)
    }

    @Provides
    fun provideBiometric(fragmentActivity: FragmentActivity): IBiometric {
        return if (Build.MANUFACTURER.toLowerCase(Locale.ROOT).contains(HUAWEI_BRAND)) BiometricHuawei(fragmentActivity)
        else BiometricGoogle(fragmentActivity)
    }

    @Provides
    fun bindNearbyManager(@NearbyHuawei huawei: INearby, @NearbyGoogle google: INearby): NearbyManager
            = NearbyManager(huawei, google)

    companion object {
        const val HUAWEI_BRAND = "huawei"
    }
}