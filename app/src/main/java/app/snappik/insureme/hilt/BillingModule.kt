package app.snappik.insureme.hilt

import androidx.fragment.app.FragmentActivity
import app.snappik.insureme.BuildConfig
import app.snappik.insureme.utils.billing.BillingGoogle
import app.snappik.insureme.utils.billing.BillingHUAWEI
import app.snappik.insureme.utils.billing.BillingManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
class BillingModule {

    @Provides
    fun provideBillingManager(fragmentActivity: FragmentActivity): BillingManager {
        return BillingManager(if(BuildConfig.FLAVOR == HUAWEI_MARKET) BillingHUAWEI(fragmentActivity)
        else BillingGoogle(fragmentActivity))
    }

    companion object{
        private const val HUAWEI_MARKET = "huawei"
    }
}