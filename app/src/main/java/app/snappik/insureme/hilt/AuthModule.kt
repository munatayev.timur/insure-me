package app.snappik.insureme.hilt

import android.app.Application
import app.snappik.insureme.InsureMeApplication
import app.snappik.insureme.utils.auth.AuthGOOGLE
import app.snappik.insureme.utils.auth.AuthHUAWEI
import app.snappik.insureme.utils.auth.AuthManager
import app.snappik.insureme.utils.auth.IAuth
import app.snappik.insureme.viewmodel.RetrofitViewModel
import app.snappik.insureme.viewmodel.UserViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class AuthGoogle

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class AuthHuawei

@Module
@InstallIn(SingletonComponent::class)
object AuthModule {

    @Provides
    @AuthHuawei
    fun bindHuaweiAuth(application: Application): IAuth = AuthHUAWEI(application as InsureMeApplication)

    @Provides
    @AuthGoogle
    fun bindGoogleAuth(application: Application,
                       retrofitViewModel: RetrofitViewModel): IAuth =
        AuthGOOGLE(application as InsureMeApplication, retrofitViewModel)

    @Singleton
    @Provides
    fun bindAuthManager(@AuthHuawei huawei: IAuth,
                        @AuthGoogle google: IAuth,
                        userViewModel: UserViewModel
    ): AuthManager = AuthManager(huawei, google, userViewModel)
}