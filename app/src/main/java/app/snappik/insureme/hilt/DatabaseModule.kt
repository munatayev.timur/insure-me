package app.snappik.insureme.hilt

import android.content.Context
import app.snappik.insureme.database.dao.FriendDao
import app.snappik.insureme.database.LocalDatabase
import app.snappik.insureme.database.dao.SettingsDao
import app.snappik.insureme.database.dao.UserDao
import app.snappik.insureme.database.repository.FriendRepository
import app.snappik.insureme.database.repository.SettingsRepository
import app.snappik.insureme.database.repository.UserRepository
import app.snappik.insureme.utils.analytics.IAnalytics
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = LocalDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideUserDao(db: LocalDatabase) = db.userDao

    @Singleton
    @Provides
    fun provideUserRepository(userDao: UserDao) = UserRepository(userDao)

    @Singleton
    @Provides
    fun provideFriendDao(db: LocalDatabase) = db.friendDao

    @Singleton
    @Provides
    fun provideFriendRepository(friendDao: FriendDao) = FriendRepository(friendDao)

    @Singleton
    @Provides
    fun provideSettingsDao(db: LocalDatabase) = db.settingsDao

    @Singleton
    @Provides
    fun provideSettingsRepository(settingsDao: SettingsDao, analytics: IAnalytics) = SettingsRepository(settingsDao, analytics)
}