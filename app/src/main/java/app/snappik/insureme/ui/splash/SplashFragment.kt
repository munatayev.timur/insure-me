package app.snappik.insureme.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractFragment
import app.snappik.insureme.databinding.FragmentSplashBinding
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.observer.IUserObserver
import app.snappik.insureme.ui.login.LoginActivity
import app.snappik.insureme.ui.main.MainActivity
import app.snappik.insureme.viewmodel.SettingsViewModel
import app.snappik.insureme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment : AbstractFragment<FragmentSplashBinding>(), IUserObserver {

    @Inject override lateinit var userViewModel: UserViewModel

    @Inject lateinit var settingsViewModel: SettingsViewModel

    override val viewId: Int = R.layout.fragment_splash

    override fun FragmentSplashBinding.initVariables(){
        fragment = this@SplashFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToUserUpdate()
        settingsViewModel.settings?.subscribe()
    }

    private fun openNext(nextActivity: Class<*>){
        Handler(Looper.getMainLooper()).postDelayed({
            onActivityFinish(nextActivity)
        }, SPLASH_ENTER_TIME)
    }

    override fun onUserFetched(user: User) = openNext(MainActivity::class.java)

    override fun noUser() = openNext(LoginActivity::class.java)

    private fun onActivityFinish(nextActivity: Class<*>) = startActivity(Intent(context, nextActivity)
            .apply { arguments?.let { putExtras(it) } }
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))

    companion object{
        private const val SPLASH_ENTER_TIME = 2000L
    }
}