package app.snappik.insureme.ui.main

import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractFragment
import app.snappik.insureme.databinding.LayoutUnlockSettingsBinding
import app.snappik.insureme.model.UnlockType
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.ui.dialog.InfoDialog
import app.snappik.insureme.ui.dialog.ListDialog
import app.snappik.insureme.ui.dialog.PinDialog
import app.snappik.insureme.viewmodel.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LockFragment : AbstractFragment<LayoutUnlockSettingsBinding>(){

    @Inject lateinit var settingsViewModel: SettingsViewModel

    override val viewId: Int = R.layout.layout_unlock_settings

    override fun LayoutUnlockSettingsBinding.initVariables() {
        lockFragment = this@LockFragment
    }

    fun chooseUnlockType(settings: Settings) = ListDialog(getString(R.string.settings_unlock_title),
            UnlockType.all(),
            UnlockType.description(),
            UnlockType.disabled(settings),
            settings.unlockType.res,
            onSelected = { settingsViewModel.updateUnlockType(UnlockType.findByRes(it)) }
    ).show(parentFragmentManager)

    fun onPinChangeClicked() = PinDialog().show(parentFragmentManager)

    fun onPanicPINCheckedChange(settings: Settings) {
        if(settings.isPremium) {
            settingsViewModel.updatePanicPin(settings.panicPIN.not() && settings.isPossibleToReversePIN())
            if (settings.isPossibleToReversePIN().not()) {
                val title = getString(R.string.general_can_not_turn_on)
                val message = getString(R.string.settings_incorrect_panic_pin)
                InfoDialog(title, message).show(parentFragmentManager)
            }
        } else featureAvailableWithPremium(R.string.settings_panic_pin, R.string.settings_panic_pin_desc)
    }

    fun onRestrictionOfUnlockChanged(settings: Settings) {
        if(settings.isPremium) { settingsViewModel.updateAttempts(if (settings.amountOfAttempts < 0) 5 else -1)
        } else featureAvailableWithPremium(R.string.settings_restrict_unlock_amount, R.string.settings_restrict_unlock_amount_desc)
    }

    override fun onBackPressed() = R.id.main_settings_layout replace SettingsFragment()
}