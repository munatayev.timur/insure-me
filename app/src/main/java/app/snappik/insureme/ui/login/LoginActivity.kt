package app.snappik.insureme.ui.login

import android.content.Intent
import android.os.Bundle
import app.snappik.insureme.common.AbstractActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AbstractActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            changeFragment(LoginFragment::class.java)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        getFragment<LoginFragment>()?.onActivityResult(requestCode, resultCode, data)
    }
}