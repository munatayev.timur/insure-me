package app.snappik.insureme.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class ICommonAdapter<T>(val list: MutableSet<T> = mutableSetOf()) : RecyclerView.Adapter<ICommonAdapter.ViewHolder<T>>() {

    open fun add(item: T){
        val listSize = list.size
        list.add(item)
        if(list.size > listSize) {
            val index = list.indexOf(item)
            notifyItemInserted(index)
        }
    }

    open fun removeAll(){
        list.clear()
        notifyDataSetChanged()
    }

    open fun remove(item: T){
        val indexOfElement = list.indexOf(item)
        list.remove(item)
        notifyItemRemoved(indexOfElement)
    }

    fun get(position: Int) = list.elementAt(position)

    abstract fun getLayoutId(): Int

    abstract fun getViewHolder(view: View): ViewHolder<T>

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) = holder.bind(list.elementAt(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<T> {
        val view = LayoutInflater.from(parent.context).inflate(getLayoutId(), parent, false)
        return getViewHolder(view)
    }

    abstract class ViewHolder<T>(val view: View, private val onClick: ((t: T) -> Unit)? = null) : RecyclerView.ViewHolder(view) {
        open fun bind(item: T) {
            view.setOnClickListener { onClick?.invoke(item) }
        }
    }
}