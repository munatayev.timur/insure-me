package app.snappik.insureme.ui.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import app.snappik.insureme.R

class ListAdapter(
    list: List<Int>,
    var disabled: List<Int>,
    private var selectedId: Int,
    private val descriptions: List<Int>,
    private val onItemSelected: (item: Int) -> Unit,
    private val onShowPremium: (item: Int, desc: Int) -> Unit,
) : ICommonAdapter<Int>(list.toMutableSet()) {

    override fun getLayoutId(): Int = R.layout.item_list_layout

    override fun getViewHolder(view: View): ViewHolder<Int> = object : ViewHolder<Int>(view){
        override fun bind(item: Int) {
            val clickable = disabled.contains(item).not()
            val mAlpha = if(clickable) 1F else 0.3F

            itemView.findViewById<TextView>(R.id.item_selection_list_text).apply {
                setText(item)
                alpha = mAlpha
            }

            itemView.findViewById<ImageView>(R.id.item_selection_list_chosen_icon).apply {
                visibility = if (selectedId == item) View.VISIBLE else View.GONE
                alpha = mAlpha
            }

            itemView.setOnClickListener {
                if(clickable){
                    if(selectedId != item) {
                        onItemSelected(item)
                        selectedId = item
                        notifyDataSetChanged()
                    }
                } else {
                    val index = list.indexOf(item)
                    onShowPremium(item, descriptions[index])
                }
            }
        }
    }
}