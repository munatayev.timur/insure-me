package app.snappik.insureme.ui.dialog

import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractDialogFragment
import app.snappik.insureme.databinding.LayoutInfoBinding

class InfoDialog(
    private val title: String,
    private val message: String,
    private val okText: Int = R.string.general_ok,
    private val cancelText: Int = R.string.general_cancel,
    private val onCancel: (() -> Unit)? = null,
    private val onOk: (() -> Unit)? = null
) :
    AbstractDialogFragment<LayoutInfoBinding>(R.layout.layout_info, R.style.SimpleDialog) {
    override fun LayoutInfoBinding.initVariables() {
        dialog = this@InfoDialog
        title = this@InfoDialog.title
        message = this@InfoDialog.message
        showCancel = onCancel != null
        okText = this@InfoDialog.okText
        cancelText = this@InfoDialog.cancelText
    }

    fun onOkClicked(){
        onOk?.invoke()
        dismiss()
    }

    fun onCancelClicked(){
        onCancel?.invoke()
        dismiss()
    }
}