package app.snappik.insureme.ui.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MotionEvent
import android.view.MotionEvent.ACTION_UP
import android.view.View
import androidx.core.content.ContextCompat
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractFragment
import app.snappik.insureme.databinding.FragmentMainBinding
import app.snappik.insureme.model.UnlockType
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.observer.IFriendObserver
import app.snappik.insureme.observer.IUserObserver
import app.snappik.insureme.service.AlarmService
import app.snappik.insureme.ui.adapter.AccountMenuAdapter
import app.snappik.insureme.ui.dialog.FriendsDialog
import app.snappik.insureme.ui.dialog.InfoDialog
import app.snappik.insureme.ui.dialog.PolicyDialog
import app.snappik.insureme.ui.splash.SplashActivity
import app.snappik.insureme.utils.ActionHelper
import app.snappik.insureme.utils.auth.AuthManager
import app.snappik.insureme.utils.biometric.IBiometric
import app.snappik.insureme.utils.push.PushTokenManager
import app.snappik.insureme.viewmodel.ActionViewModel
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.FALL_DETECTED
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.NONE
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.REQUIRED
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.SETTINGS
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.TIME
import app.snappik.insureme.viewmodel.FriendViewModel
import app.snappik.insureme.viewmodel.RetrofitViewModel
import app.snappik.insureme.viewmodel.SettingsViewModel
import app.snappik.insureme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class MainFragment : AbstractFragment<FragmentMainBinding>(),
        IUserObserver, IFriendObserver {

    @Inject lateinit var biometric: IBiometric

    @Inject lateinit var authManager: AuthManager

    @Inject lateinit var pushManager: PushTokenManager

    @Inject lateinit var actionHelper: ActionHelper

    @Inject override lateinit var friendViewModel: FriendViewModel

    @Inject override lateinit var userViewModel: UserViewModel

    @Inject lateinit var settingsViewModel: SettingsViewModel

    @Inject lateinit var actionViewModel: ActionViewModel

    @Inject lateinit var retrofitViewModel: RetrofitViewModel

    override val viewId: Int = R.layout.fragment_main

    override fun FragmentMainBinding.initVariables() {
        fragment = this@MainFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToUserUpdate()
        subscribeToFriendsUpdate()
        subscribeToActionsUpdate()
    }

    private fun subscribeToActionsUpdate() = actionViewModel.action.subscribe {
        activity?.window?.navigationBarColor = ContextCompat.getColor(requireContext(), when(it){
            TIME, REQUIRED, FALL_DETECTED -> R.color.background
            SETTINGS -> R.color.button_red
            else -> R.color.button
        })
    }

    fun openSettings() = R.id.main_settings_layout replace SettingsFragment()

    override fun onPermissionsGranted() {
        val intent = Intent(requireContext(), AlarmService::class.java)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            activity?.startForegroundService(intent)
        } else activity?.startService(intent)
    }

    override fun onSettingsRequested() = openSettings()

    fun onStartClicked(settings: Settings, friends: List<Friend>?) {
        getParentActivity<MainActivity>()?.requestPermissionAndCheck(settings, friends, this)
    }

    fun onBackgroundTouched(event: MotionEvent, settings: Settings): Boolean {
        if (event.action == ACTION_UP && actionViewModel.isTicking() && settings.unlockType == UnlockType.BIOMETRIC) {
            biometric.onSuccess = { actionViewModel.unlockWithBiometrics(settings) }
            biometric.auth(this, ContextCompat.getMainExecutor(context))
        }
        return true
    }

    override fun onBackPressed() = parentFragmentManager.fragments
        .filterIsInstance<AbstractFragment<*>>()
        .lastOrNull()?.let {
        (it as? AbstractFragment<*>)?.onBackPressed()
    } ?: Unit

    override fun onUserFetched(user: User) {
        if (user.isAgreedWithTerms.not()) {
            Handler(Looper.getMainLooper()).postDelayed({
                PolicyDialog(true) {
                    userViewModel.applyTermsAndConditions(user.apply { isAgreedWithTerms = true })
                }.show(parentFragmentManager)
            }, TERMS_DELAY)
        }
    }

    override fun onFriendsFetched(list: List<Friend>) {
        actionHelper.friends = list
    }

    fun getAccountAdapter(user: User?) = AccountMenuAdapter(requireContext(),
        user?.displayName ?: "",
        user?.type ?: User.Type.GUEST,
        onFriends = {
            FriendsDialog(false){
                friendViewModel.addFriendToSend(it)
            }.show(childFragmentManager)
        },
        notAvailableForGuest = {
            InfoDialog(
                getString(R.string.general_not_available),
                getString(R.string.login_not_available_for_guest)
            ).show(this@MainFragment.childFragmentManager)
        },
        onSettings = { openSettings() },
        onAbout = { PolicyDialog(false).show(parentFragmentManager) },
        onLogout = { cleanData(user) })

    fun getState(action: ActionViewModel.ActionStatus) = when(action){
        TIME -> R.id.timer
        REQUIRED, FALL_DETECTED -> R.id.action
        SETTINGS -> R.id.settings
        else -> R.id.main
    }

    private fun cleanData(user: User?){
        if(user == null) return

        fun commonRemove(){
            settingsViewModel.nullifySettings()
            userViewModel.deleteUser {
                startActivity(
                    Intent(activity, SplashActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
            }
        }

        if(user.type == User.Type.GUEST) commonRemove() else {
            authManager.getAuth()?.signOut {
                pushManager.getPushService(user).unsubscribeToTopic(user)
                friendViewModel.deleteAll()
                commonRemove()
            }
        }
    }

    companion object {
        private const val TERMS_DELAY = 1500L
    }
}