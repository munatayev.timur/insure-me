package app.snappik.insureme.ui.dialog

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.NumberPicker
import app.snappik.insureme.BR
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractDialogFragment
import app.snappik.insureme.databinding.LayoutNumberPickerBinding
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.observer.ISettingsObserver
import app.snappik.insureme.viewmodel.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class NumberPickerDialog :
    AbstractDialogFragment<LayoutNumberPickerBinding>(
        R.layout.layout_number_picker,
        R.style.SimpleDialog
    ), ISettingsObserver {

    @Inject override lateinit var settingsViewModel: SettingsViewModel

    override fun LayoutNumberPickerBinding.initVariables() {
        dialog = this@NumberPickerDialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        fetchSettings()
    }

    fun onOkClicked(delay_picker: NumberPicker) {
        settingsViewModel.updateDelay(delay_picker.value * 1000 * 60L)
        dismiss()
    }

    override fun onSettingsFetched(settings: Settings) {
        BR.realValue.updateWith {
            realValue = (settings.delay / 1000 / 60).toInt()
        }
    }

    fun getDisplayValues() = Array(60){ String.format(getString(R.string.general_min), it + 1) }
}