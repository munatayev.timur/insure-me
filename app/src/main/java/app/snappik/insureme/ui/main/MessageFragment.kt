package app.snappik.insureme.ui.main

import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractFragment
import app.snappik.insureme.databinding.LayoutDataDeliverySettingsBinding
import app.snappik.insureme.model.Strategy
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.ui.dialog.FriendsDialog
import app.snappik.insureme.ui.dialog.InputTextDialog
import app.snappik.insureme.ui.dialog.SelectionListDialog
import app.snappik.insureme.viewmodel.FriendViewModel
import app.snappik.insureme.viewmodel.SettingsViewModel
import app.snappik.insureme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MessageFragment : AbstractFragment<LayoutDataDeliverySettingsBinding>() {

    override val viewId: Int = R.layout.layout_data_delivery_settings

    @Inject
    lateinit var settingsViewModel: SettingsViewModel

    @Inject
    lateinit var friendViewModel: FriendViewModel

    @Inject
    lateinit var userViewModel: UserViewModel

    override fun LayoutDataDeliverySettingsBinding.initVariables() {
        fragment = this@MessageFragment
    }

    fun onOpenInputTextDialog(mType: InputTextDialog.Type) =
        InputTextDialog(mType).show(parentFragmentManager)

    fun pickStrategy(settings: Settings, user: User) = SelectionListDialog(
        getString(R.string.settings_message_title),
        Strategy.all(),
        Strategy.disabledPremium(settings),
        Strategy.disabledGuest(user),
        Strategy.description(),
        settings.strategies.map { it.res },
        onItemUnSelected = {
            settingsViewModel.updateStrategies(
                settings.strategies.apply { remove(Strategy.findByRes(it)) })
        },
        onItemSelected = {
            settingsViewModel.updateStrategies(
                settings.strategies.apply { add(Strategy.findByRes(it)) })
        }).show(parentFragmentManager)

    fun onSendLocationChanged(settings: Settings) {
        if (settings.isPremium) {
            settingsViewModel.updateSendLocation(settings.sendLocation.not())
        } else featureAvailableWithPremium(
            R.string.email_include_location,
            R.string.email_include_location_desc
        )
    }

    fun onChooseDestinationDevice() = FriendsDialog(true){
        friendViewModel.addFriendToSend(it)
    }.show(childFragmentManager)

    override fun onBackPressed() = R.id.main_settings_layout replace SettingsFragment()
}
