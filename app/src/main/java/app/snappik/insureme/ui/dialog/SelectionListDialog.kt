package app.snappik.insureme.ui.dialog

import androidx.recyclerview.widget.LinearLayoutManager
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractDialogFragment
import app.snappik.insureme.databinding.LayoutListBinding
import app.snappik.insureme.ui.adapter.SelectionListAdapter

class SelectionListDialog(
        private val title: String,
        private val list: List<Int>,
        private val disabledPremium: List<Int>,
        private val disabledGuest: List<Int>,
        private val description: List<Int>,
        private val selected: List<Int>,
        private val onItemSelected: (item: Int) -> Unit,
        private val onItemUnSelected: (item: Int) -> Unit
) : AbstractDialogFragment<LayoutListBinding>(R.layout.layout_list, R.style.SimpleDialog) {

    override fun LayoutListBinding.initVariables() {
        title = this@SelectionListDialog.title
        dialog = this@SelectionListDialog
        manager = LinearLayoutManager(context)
        adapter = SelectionListAdapter(list, disabledPremium, disabledGuest, selected, description,
        onShowPremium = { res, desc -> onShowPremium(res, desc) },
        onShowDisabledForGuest = { featureNotAvailableAsGuest() },
        onItemSelected, onItemUnSelected)
    }

    private val onPremiumUpdate = fun(){
        (viewDataBinding.adapter as? SelectionListAdapter)?.disabledPremium = emptyList()
        viewDataBinding.invalidateAll()
    }

    private fun onShowPremium(res: Int, desc: Int) = featureAvailableWithPremium(res, desc, title, onPremiumUpdate)
}