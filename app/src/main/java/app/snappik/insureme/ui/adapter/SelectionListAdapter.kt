package app.snappik.insureme.ui.adapter

import android.view.View
import android.widget.CheckBox
import app.snappik.insureme.R

class SelectionListAdapter(list: List<Int>,
                           var disabledPremium: List<Int>,
                           var disabledGuest: List<Int>,
                           private val selected: List<Int>,
                           private val descriptions: List<Int>,
                           private val onShowPremium: (item: Int, desc: Int) -> Unit,
                           private val onShowDisabledForGuest: () -> Unit,
                           private val onItemSelected: (item: Int) -> Unit,
                           private val onItemUnSelected: (item: Int) -> Unit) :
    ICommonAdapter<Int>(list.toMutableSet()) {

    override fun getLayoutId(): Int = R.layout.item_selection_list_layout

    override fun getViewHolder(view: View): ViewHolder<Int> = object : ViewHolder<Int>(view){
        override fun bind(item: Int) {
            val isClickablePremium = disabledPremium.contains(item).not()
            val isClickableGuest = disabledGuest.contains(item).not()
            view.findViewById<CheckBox>(R.id.item_selection_list_chosen_icon).apply {
                isChecked = selected.contains(item)
                setText(item)
                view.setOnClickListener { isChecked = isChecked.not() }
                alpha = if(isClickablePremium && isClickableGuest) 1F else 0.3F
                setOnCheckedChangeListener { _, isChecked ->
                    if(isChecked){
                        val index = list.indexOf(item)
                        this.isChecked = if(isClickablePremium && isClickableGuest) isChecked else false
                        when {
                            isClickablePremium.not() -> onShowPremium.invoke(item, descriptions[index])
                            isClickableGuest.not() -> onShowDisabledForGuest.invoke()
                            else -> onItemSelected(item)
                        }
                    } else onItemUnSelected(item)
                }
            }
        }
    }
}