package app.snappik.insureme.ui.dialog

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.app.NotificationManagerCompat.IMPORTANCE_HIGH
import androidx.recyclerview.widget.LinearLayoutManager
import app.snappik.insureme.BR
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractDialogFragment
import app.snappik.insureme.databinding.DialogFriendsBinding
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.model.entity.friend
import app.snappik.insureme.model.request.GooglePushRequest
import app.snappik.insureme.model.request.HuaweiPushRequest
import app.snappik.insureme.observer.IFriendObserver
import app.snappik.insureme.observer.IUserObserver
import app.snappik.insureme.ui.adapter.FriendsAdapter
import app.snappik.insureme.ui.adapter.NearByFriendsAdapter
import app.snappik.insureme.ui.main.MainActivity
import app.snappik.insureme.utils.Utilities.getMD5
import app.snappik.insureme.utils.auth.AuthManager
import app.snappik.insureme.utils.nearby.NearbyManager
import app.snappik.insureme.utils.notification.INotification
import app.snappik.insureme.viewmodel.FriendViewModel
import app.snappik.insureme.viewmodel.RetrofitViewModel
import app.snappik.insureme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class FriendsDialog(private val isChooser: Boolean = false,
                    private val onChoose: ((friend: Friend) -> Unit)? = null) :
        AbstractDialogFragment<DialogFriendsBinding>(
                R.layout.dialog_friends,
                R.style.SimpleDialog
        ), IUserObserver, IFriendObserver {

    @Inject
    override lateinit var userViewModel: UserViewModel

    @Inject
    override lateinit var friendViewModel: FriendViewModel

    @Inject
    lateinit var nearbyManager: NearbyManager

    @Inject
    lateinit var authManager: AuthManager

    @Inject
    lateinit var retrofitViewModel: RetrofitViewModel

    private val saveFriend = fun(name: String, token: String) {
        friendViewModel.insert(name, token)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channelName = "$name messages channel"
            val nManager = activity?.getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager
            nManager?.let { INotification.createNotificationChannel(it, token, channelName, IMPORTANCE_HIGH) }
        }
    }

    init {
        isCancelable = false
    }

    override fun DialogFriendsBinding.initVariables() {
        dialogScanner = this@FriendsDialog
        emptyNewDevices = true
        emptyOldDevices = true
        isChooser = this@FriendsDialog.isChooser
        oldFriendsLayoutManager = LinearLayoutManager(context)
        oldFriendsAdapter = FriendsAdapter(isChooser,
            onRename = { InputTextDialog(InputTextDialog.Type.RENAME_FRIEND, it).show(parentFragmentManager) },
            onAction = { friend ->
                friendViewModel.delete(friend)
                authManager.getAuth()?.silentSignIn { accessToken ->
                    userViewModel.user?.value?.let { send(it, friend, accessToken) }
                }
            },
            onChoose = {
                this@FriendsDialog.onChoose?.invoke(it)
                dismiss()
            })
        newFriendsLayoutManager = LinearLayoutManager(context)
        newFriendsAdapter = NearByFriendsAdapter(isChooser, onAction = { nearbyManager.getNearby()?.connect(it.token, saveFriend) })
    }

    private fun send(user: User, friend: Friend, accessToken : String) = CoroutineScope(Dispatchers.IO).launch {
        if(user.type == User.Type.HUAWEI){
            retrofitViewModel.sendHuaweiPush(requireContext(), accessToken, HuaweiPushRequest.createForDelete(friend.token, user.displayName.getMD5()))
        } else if(user.type == User.Type.GOOGLE){
            retrofitViewModel.sendGooglePush(requireContext(), accessToken, GooglePushRequest.createToDelete(friend.token, user.displayName.getMD5()))
        }
    }

    override fun onPermissionsGranted() {
        if(isChooser.not()) subscribeToUserUpdate()
        subscribeToFriendsUpdate()
    }

    override fun onPermissionRejected(activity: Activity) {
        super.onPermissionRejected(activity)
        dismiss()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? MainActivity)?.requestPermissions(this, ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)
    }

    override fun onUserFetched(user: User) {
        nearbyManager.user = user
        startDiscovery()
        startAdvertising(user)
    }

    private fun startAdvertising(user: User) = nearbyManager.getNearby()?.startAdvertising(user.displayName, user.displayName.getMD5(), onDevicePaired = saveFriend)

    override fun onFriendsFetched(list: List<Friend>) = viewDataBinding.run {
        BR.emptyOldDevices.updateWith {
            emptyOldDevices = list.isEmpty()
        }
        BR.oldFriendsAdapter.updateWith {
            oldFriendsAdapter?.removeAll()
            list.forEach {
                oldFriendsAdapter?.add(it)
                newFriendsAdapter?.removeByName(it.displayName){
                    BR.emptyNewDevices.updateWith{
                        emptyNewDevices = true
                    }
                }
            }
        }
    }

    private fun startDiscovery() = viewDataBinding.run {
        nearbyManager.getNearby()?.startDiscovery(
            onScannerStart = { },
            onDeviceFound = { id, name ->
                if (oldFriendsAdapter?.contains(name)?.not() == true) {
                    BR.emptyNewDevices.updateWith{
                        emptyNewDevices = false
                    }
                    BR.newFriendsAdapter.updateWith {
                        val newFriend = friend {
                            displayName = name
                            token = id
                        }
                        newFriendsAdapter?.add(newFriend)
                    }
                }
            },
            onDeviceLost = { id ->
                BR.newFriendsAdapter.updateWith {
                    newFriendsAdapter?.removeByToken(id) {
                        BR.emptyNewDevices.updateWith{
                            emptyNewDevices = true
                        }
                    }
                }
            })
    }

    override fun onDestroy() {
        super.onDestroy()
        nearbyManager.getNearby()?.stopAdvertising()
        nearbyManager.getNearby()?.stopDiscovering()
        nearbyManager.getNearby()?.disconnectAll()
    }
}