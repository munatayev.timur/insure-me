package app.snappik.insureme.ui.map

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractFragment
import app.snappik.insureme.databinding.FragmentMapBinding
import app.snappik.insureme.ui.main.MainActivity
import app.snappik.insureme.utils.action.PushAction.Companion.BODY
import app.snappik.insureme.utils.action.PushAction.Companion.DISPLAY_NAME
import app.snappik.insureme.utils.action.PushAction.Companion.LATITUDE
import app.snappik.insureme.utils.action.PushAction.Companion.LONGITUDE
import app.snappik.insureme.utils.map.IMap
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MapFragment : AbstractFragment<FragmentMapBinding>() {

    @Inject lateinit var map: IMap

    override val viewId: Int = R.layout.fragment_map

    override fun FragmentMapBinding.initVariables(){
        fragment = this@MapFragment
        body = arguments?.getString(BODY)
        displayName = arguments?.getString(DISPLAY_NAME)
        val lat = arguments?.getString(LATITUDE)
        val lon = arguments?.getString(LONGITUDE)
        lat?.let { latLong = "$lat $lon" }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<FrameLayout>(R.id.mapContainer).addView(map.mapView)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        map.onCreate()
        loadMap()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        map.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    private fun loadMap() {
        val lat = arguments?.getString(LATITUDE)
        val lon = arguments?.getString(LONGITUDE)
        if(lat.isNullOrEmpty().not() && lon.isNullOrEmpty().not()){
            map.loadMap(lat ?: "", lon ?: "")
        }
    }

    fun onBackButtonPressed() = startActivity(Intent(context, MainActivity::class.java)
        .apply { arguments?.let { putExtras(it) } }
        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
}