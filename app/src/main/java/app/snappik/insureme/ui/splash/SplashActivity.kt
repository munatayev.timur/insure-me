package app.snappik.insureme.ui.splash

import android.os.Bundle
import app.snappik.insureme.common.AbstractActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : AbstractActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            changeFragment(SplashFragment::class.java)
        }
    }
}