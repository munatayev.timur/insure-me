package app.snappik.insureme.ui.main

import android.content.Intent
import android.os.Bundle
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractActivity
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.observer.IAbstractObserver
import app.snappik.insureme.observer.ISettingsObserver
import app.snappik.insureme.service.FallDetectorService
import app.snappik.insureme.ui.dialog.BillingDialog
import app.snappik.insureme.ui.dialog.InfoDialog
import app.snappik.insureme.utils.AlarmHelper
import app.snappik.insureme.utils.analytics.IAnalytics
import app.snappik.insureme.utils.billing.BillingManager
import app.snappik.insureme.utils.biometric.IBiometric
import app.snappik.insureme.utils.permissions.IPermissions
import app.snappik.insureme.viewmodel.FriendViewModel
import app.snappik.insureme.viewmodel.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AbstractActivity(), IAbstractObserver, ISettingsObserver{

    @Inject lateinit var biometric: IBiometric

    @Inject lateinit var billingManager: BillingManager

    @Inject lateinit var permissions: IPermissions

    @Inject lateinit var friendViewModel: FriendViewModel

    @Inject lateinit var analytics: IAnalytics

    @Inject lateinit var alarmHelper: AlarmHelper

    @Inject override lateinit var settingsViewModel: SettingsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        billingManager.subscriptionListener = onSubscriptionStatusChanged
        if (savedInstanceState == null) {
            changeFragment(MainFragment::class.java)
            billingManager.initConnection()
        }
        fetchSettings()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        permissions.onActivityResult(requestCode, resultCode)
        billingManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSettingsFetched(settings: Settings) = FallDetectorService.checkAndStart(this)

    fun showBillingDialog(onBought: () -> Unit) =
        billingManager.subscriptionDetails.value?.let { product ->
            analytics.subscriptionRead()
            BillingDialog(product)
            {
              billingManager.purchase(product)
            }.also { dialog ->
                billingManager.billingFinished.observeOnce { finished ->
                    finished?.let {
                        if(it) {
                            dialog.dismiss()
                            onBought.invoke()
                        }
                    }
                }
            }.show(supportFragmentManager)
        } ?: InfoDialog(getString(R.string.billing_general_title),
            getString(R.string.billing_can_not_launch_purchase), onOk = {}
        ).show(supportFragmentManager)

    fun requestPermissionAndCheck(settings: Settings,
                                  friends: List<Friend>?,
                                  source: IPermissions.Callback){
        val selectedFriends = friends?.filter { it.selected } ?: emptyList()
        val errors = settings.getIncorrectSettings(biometric.canAuthWithFingerprint(), selectedFriends)
        if (errors.isEmpty()) {
            val permissionsForSettings = settings.getPermissions()
            requestPermissions(source, *permissionsForSettings.toTypedArray())
            if (settings.sendLocation && settings.isLocationServiceEnabled(this).not()) {
                InfoDialog(
                    getString(R.string.permissions_location_disabled_title),
                    getString(R.string.permissions_location_disabled_desc)
                ).show(supportFragmentManager)
            }
        } else {
            val title = getString(R.string.settings_incorrect_errors_title)
            val message = errors.map { "${getString(it)}<br></br>" }.toString()
                .replace("[", " - ")
                .replace("]", "")
                .replace(", ", " - ")
            InfoDialog(title, message, R.string.settings_open, onOk = {
                source.onSettingsRequested()
            }).show(supportFragmentManager)
        }
    }

    fun requestPermissions(source: IPermissions.Callback, vararg newComePermission: String){
        permissions.checkPermissions(source, *newComePermission)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        mPermissions: Array<String>,
        grantResults: IntArray
    ) {
        permissions.onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onBackPressed() {
        if(supportFragmentManager.fragments.size == 1) super.onBackPressed()
        else getFragment<MainFragment>()?.onBackPressed()
    }

    private val onSubscriptionStatusChanged = fun(isPremium: Boolean){
        settingsViewModel.updateSubscriptionStatus(isPremium)
    }
}