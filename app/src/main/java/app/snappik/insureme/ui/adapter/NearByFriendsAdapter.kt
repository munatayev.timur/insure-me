package app.snappik.insureme.ui.adapter

import app.snappik.insureme.model.entity.Friend

class NearByFriendsAdapter(
    override val isChooser: Boolean,
    override val onAction: (friend: Friend) -> Unit) : FriendsAdapter(isChooser, onAction){

    fun removeByToken(endpointId: String, noDevicesLeft: () -> Unit){
        list.find { it.token == endpointId }
            ?.let { friend ->  super.remove(friend) }
        if(list.isEmpty()) noDevicesLeft.invoke()
    }

    fun removeByName(name: String, noDevicesLeft: () -> Unit){
        list.find { it.displayName == name }
            ?.let { friend ->  super.remove(friend) }
        if(list.isEmpty()) noDevicesLeft.invoke()
    }
}