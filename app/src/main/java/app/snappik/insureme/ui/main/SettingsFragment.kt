package app.snappik.insureme.ui.main

import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractFragment
import app.snappik.insureme.databinding.LayoutSettingsBinding
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.viewmodel.ActionViewModel
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.NONE
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.SETTINGS
import app.snappik.insureme.viewmodel.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SettingsFragment : AbstractFragment<LayoutSettingsBinding>(){

    override val viewId: Int = R.layout.layout_settings

    @Inject lateinit var settingsViewModel: SettingsViewModel

    @Inject lateinit var actionViewModel: ActionViewModel

    override fun LayoutSettingsBinding.initVariables() {
        settingsFragment = this@SettingsFragment
    }

    override fun onResume() {
        super.onResume()
        actionViewModel.setAction(SETTINGS)
    }

    fun showBuyDialog(settings: Settings){
        if(settings.isPremium) return
        getParentActivity<MainActivity>()?.showBillingDialog {
            viewDataBinding.invalidateAll()
        }
    }

    fun openAlarmSettings() = R.id.main_settings_layout replace TimeFragment()

    fun openUnlockSettings() = R.id.main_settings_layout replace LockFragment()

    fun openDataDelivery() = R.id.main_settings_layout replace MessageFragment()

    fun openEvents() = R.id.main_settings_layout replace EventsFragment()

    override fun onBackPressed(){
        actionViewModel.setAction(NONE)
        closeFragment()
    }
}