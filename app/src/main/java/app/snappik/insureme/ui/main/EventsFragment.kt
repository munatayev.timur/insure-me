package app.snappik.insureme.ui.main

import android.os.Bundle
import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractFragment
import app.snappik.insureme.databinding.LayoutEventsBinding
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.service.FallDetectorService
import app.snappik.insureme.utils.AlarmHelper
import app.snappik.insureme.viewmodel.FriendViewModel
import app.snappik.insureme.viewmodel.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class EventsFragment : AbstractFragment<LayoutEventsBinding>() {

    @Inject lateinit var settingsViewModel: SettingsViewModel

    @Inject lateinit var friendViewModel: FriendViewModel

    @Inject lateinit var alarmHelper: AlarmHelper

    override val viewId: Int = R.layout.layout_events

    override fun LayoutEventsBinding.initVariables() {
        eventsFragment = this@EventsFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<MotionLayout>(R.id.events_settings_root).transitionToState(R.id.events_open)
    }

    fun onFallDetectorChanged(settings: Settings, friends: List<Friend>?){
        getParentActivity<MainActivity>()?.requestPermissionAndCheck(settings, friends, this)
    }

    override fun onSettingsRequested() = onBackPressed()

    override fun onPermissionsGranted(){
        settingsViewModel.updateFallDetector(settingsViewModel.settings?.value?.fallDetector?.not() ?: false)
        FallDetectorService.checkAndStart(requireContext())
    }

    override fun onBackPressed() = R.id.main_settings_layout replace SettingsFragment()

    companion object {
        const val CHECK_FALL_DETECTOR_TIME = 60 * 60 * 1000L
    }
}