package app.snappik.insureme.ui.dialog

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractDialogFragment
import app.snappik.insureme.databinding.LayoutInputTextBinding
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.utils.Utilities.showKeyboard
import app.snappik.insureme.viewmodel.FriendViewModel
import app.snappik.insureme.viewmodel.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class InputTextDialog(private val type: Type, private var friendToRename: Friend? = null) :
    AbstractDialogFragment<LayoutInputTextBinding>(
        R.layout.layout_input_text,
        R.style.SimpleDialog
    ) {

    @Inject lateinit var settingsViewModel: SettingsViewModel

    @Inject lateinit var friendsViewModel: FriendViewModel

    override fun LayoutInputTextBinding.initVariables() {
        dialog = this@InputTextDialog
        type = this@InputTextDialog.type
        context = this@InputTextDialog.context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.input_text_container).run {
            post { showKeyboard(this) }
        }
    }

    fun onOkClicked(container: EditText, emailSurname: EditText) {
        val data = container.text.toString()
        when (type) {
            Type.RENAME_FRIEND -> friendToRename?.let { friendsViewModel.updateFriend(it.apply { displayName = data }) }
            Type.SMS_MESSAGE -> settingsViewModel.updateMessage(data)
            Type.PHONE_NUMBER -> settingsViewModel.updateDestinationNumber(data)
            else -> settingsViewModel.updateEmailSurname(data, emailSurname.text.toString())
        }
        dismiss()
    }

    enum class Type(val titleRes: Int, val descRes: Int, val hint: Int) {
        RENAME_FRIEND(
            R.string.input_dialog_rename_friend,
            R.string.input_dialog_rename_friend_description,
            R.string.input_dialog_rename_friend_hint
        ),
        SMS_MESSAGE(
            R.string.input_dialog_sms,
            R.string.input_dialog_sms_description,
            R.string.input_dialog_sms_hint
        ),
        PHONE_NUMBER(
            R.string.input_dialog_phone_number,
            R.string.input_dialog_phone_number_description,
            R.string.input_dialog_phone_number_hint
        ),
        EMAIL(
            R.string.input_dialog_email,
            R.string.input_dialog_email_description,
            R.string.input_dialog_email_hint
        )
    }
}