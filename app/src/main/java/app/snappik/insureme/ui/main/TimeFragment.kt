package app.snappik.insureme.ui.main

import android.app.TimePickerDialog
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractFragment
import app.snappik.insureme.databinding.LayoutAlarmSettingsBinding
import app.snappik.insureme.model.AlarmType
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.ui.dialog.ListDialog
import app.snappik.insureme.ui.dialog.NumberPickerDialog
import app.snappik.insureme.viewmodel.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.Calendar
import javax.inject.Inject

@AndroidEntryPoint
class TimeFragment : AbstractFragment<LayoutAlarmSettingsBinding>() {

    override val viewId: Int = R.layout.layout_alarm_settings

    @Inject lateinit var settingsViewModel: SettingsViewModel

    override fun LayoutAlarmSettingsBinding.initVariables() {
        timeFragment = this@TimeFragment
    }

    fun pickAlarmType(settings: Settings) = ListDialog(
            getString(R.string.alarm_type_title),
            AlarmType.all(),
            AlarmType.description(),
            AlarmType.disabled(settings),
            settings.alarmType.res,
            onSelected = { settingsViewModel.updateAlarmType(AlarmType.findByRes(it)) }
    ).show(parentFragmentManager)

    fun showTimePicker() {
        val cal = Calendar.getInstance()
        val timeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            settingsViewModel.updateTime(cal.timeInMillis)
        }
        TimePickerDialog(context, R.style.TimePickerDialog, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(
                Calendar.MINUTE), true).show()
    }

    fun pickDelay() = NumberPickerDialog().show(parentFragmentManager)

    override fun onBackPressed() = R.id.main_settings_layout replace SettingsFragment()
}