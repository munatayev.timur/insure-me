package app.snappik.insureme.ui.dialog

import android.graphics.Color
import app.snappik.insureme.BR
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractDialogFragment
import app.snappik.insureme.databinding.LayoutPinBinding
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.observer.ISettingsObserver
import app.snappik.insureme.utils.view.IPinEvent
import app.snappik.insureme.viewmodel.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PinDialog : AbstractDialogFragment<LayoutPinBinding>(R.layout.layout_pin, R.style.SimpleDialog),
        IPinEvent, ISettingsObserver {

    @Inject override lateinit var settingsViewModel: SettingsViewModel

    override fun LayoutPinBinding.initVariables() {
        pinlayout = this@PinDialog
    }

    override fun add(text: String) {
        BR.pinText.updateWith {
            pinText = "${pinText ?: ""}${text}"
        }
    }

    override fun onOkClicked(text: String?) {
        if(text.isNullOrEmpty()) return
        fetchSettings()
    }

    override fun onSettingsFetched(settings: Settings) {
        BR.pinText.updateWith {
            checkReversePIN(pinText ?: "", settings)
        }
    }

    override fun onTextChanged(s: CharSequence, before: Int, after: Int, count: Int) = Unit

    override fun isShowControls(): Boolean = true

    override fun onRemoveClicked() = BR.pinText.updateWith { pinText = "" }

    override fun getTextColor(): Int = Color.BLACK

    private fun checkReversePIN(newPin: String, settings: Settings) {
        if (newPin == newPin.reversed() && settings.panicPIN) {
            val title = getString(R.string.general_continue_question)
            val message = getString(R.string.panic_pin_change_to_irreversible_pin)
            InfoDialog(title, message, onCancel = { dismiss() },
                onOk = {
                    settingsViewModel.updatePanicPin(false)
                    saveAndClose(newPin)
                }).show(parentFragmentManager)
        } else saveAndClose(newPin)
    }

    private fun saveAndClose(newPin: String) {
        settingsViewModel.updatePin(newPin)
        dismiss()
    }
}