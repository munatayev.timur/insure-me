package app.snappik.insureme.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractFragment
import app.snappik.insureme.databinding.FragmentLoginBinding
import app.snappik.insureme.model.entity.User
import app.snappik.insureme.observer.IUserObserver
import app.snappik.insureme.ui.dialog.InfoDialog
import app.snappik.insureme.ui.main.MainActivity
import app.snappik.insureme.utils.auth.AuthManager
import app.snappik.insureme.utils.push.PushTokenManager
import app.snappik.insureme.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : AbstractFragment<FragmentLoginBinding>(), IUserObserver {

    @Inject override lateinit var userViewModel: UserViewModel

    @Inject lateinit var authManager: AuthManager

    @Inject lateinit var pushManager: PushTokenManager

    private val next = fun(){
        startActivity(Intent(context, MainActivity::class.java)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    or Intent.FLAG_ACTIVITY_REORDER_TO_FRONT))
    }

    override val viewId: Int = R.layout.fragment_login

    override fun FragmentLoginBinding.initVariables() { fragment = this@LoginFragment }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToUserUpdate()
    }

    override fun onUserFetched(user: User) {
        if (user.type == User.Type.GUEST) next.invoke() else
            pushManager.getPushService(user).initSubscription(
                user,
                success = { next.invoke() },
                failure = {
                    authManager.getAuth()?.signOut {
                        userViewModel.deleteUser {
                            InfoDialog(
                                "Firebase messaging error",
                                "Can not subscribe to topic"
                            ).show(childFragmentManager)
                        }
                    }
                }
            )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        authManager.onActivityResult(requestCode, data)
    }
}
