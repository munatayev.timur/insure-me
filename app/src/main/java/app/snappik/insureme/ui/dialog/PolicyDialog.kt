package app.snappik.insureme.ui.dialog

import android.os.Bundle
import android.widget.CompoundButton
import app.snappik.insureme.BR
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractDialogFragment
import app.snappik.insureme.databinding.LayoutPolicyBinding
import app.snappik.insureme.utils.view.CustomScrollView

class PolicyDialog(
    private val withPolicy: Boolean,
    private val onAgreedAndClothed: (() -> Unit)? = null
) : AbstractDialogFragment<LayoutPolicyBinding>(
    R.layout.layout_policy,
    if (withPolicy) R.style.FullScreenDialog else R.style.SimpleDialog
), CompoundButton.OnCheckedChangeListener,
    CustomScrollView.OnBottomReachedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = withPolicy.not()
    }

    override fun LayoutPolicyBinding.initVariables() {
        dialog = this@PolicyDialog
        withPolicy = this@PolicyDialog.withPolicy
        isClosable = withPolicy?.not() ?: false
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) =
            BR.isClosable.updateWith { isClosable = isChecked }

    override fun onBottomReached() = BR.isScrolled.updateWith { isScrolled = true }

    fun onOkClicked() {
        onAgreedAndClothed?.invoke()
        dismiss()
    }
}