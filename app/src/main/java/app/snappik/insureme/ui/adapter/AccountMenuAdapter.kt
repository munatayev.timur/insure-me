package app.snappik.insureme.ui.adapter

import android.content.Context
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import app.snappik.insureme.R
import app.snappik.insureme.model.AccountMenu
import app.snappik.insureme.model.AccountMenu.Companion.flattenDisplayName
import app.snappik.insureme.model.entity.User

class AccountMenuAdapter(context: Context,
                         val displayName: String,
                         private val accountType: User.Type,
                         private val onFriends: () -> Unit,
                         private val notAvailableForGuest: () -> Unit,
                         private val onSettings: () -> Unit,
                         private val onAbout: () -> Unit,
                         private val onLogout: () -> Unit) :
    ArrayAdapter<String>(context, 0, flattenDisplayName(displayName, context)) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View =
        convertView ?: LayoutInflater.from(context).inflate(R.layout.item_account_menu_s, parent, false).also {
            getItem(position)?.let { item -> setAccountItemMenu(it, accountType, item, position) }
        }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val viewId = if (position == 0) R.layout.item_account_menu_s else R.layout.item_account_menu
        return LayoutInflater.from(context).inflate(viewId, parent, false).also { root ->
            getItem(position)?.let { item -> setAccountItemMenu(root, accountType, item, position) }
            root.setOnClickListener {
                when (position.minus(1)) {
                    AccountMenu.FRIENDS.ordinal -> {
                        if (accountType == User.Type.GUEST) notAvailableForGuest.invoke() else onFriends.invoke()
                    }
                    AccountMenu.SETTINGS.ordinal -> {
                        onSettings.invoke()
                    }
                    AccountMenu.ABOUT.ordinal -> {
                        onAbout.invoke()
                    }
                    AccountMenu.LOGOUT.ordinal -> {
                        onLogout.invoke()
                    }
                }
                parent.rootView.dispatchKeyEvent(
                    KeyEvent(
                        KeyEvent.ACTION_DOWN,
                        KeyEvent.KEYCODE_BACK
                    )
                )
                parent.rootView.dispatchKeyEvent(
                    KeyEvent(
                        KeyEvent.ACTION_UP,
                        KeyEvent.KEYCODE_BACK
                    )
                )
            }
            if (position.minus(1) == AccountMenu.FRIENDS.ordinal && accountType == User.Type.GUEST) {
                root.alpha = 0.3F
            }
        }
    }

    private fun setAccountItemMenu(view: View, type: User.Type, item: String, position: Int) {
        val acTypeRs = when(type){
            User.Type.GUEST -> R.drawable.ic_guest
            User.Type.GOOGLE -> R.drawable.googleg_standard_color_18
            User.Type.HUAWEI -> R.drawable.hwid_auth_button_white
        }
        view.findViewById<TextView>(R.id.sr_text).text = item
        if(position == 0) view.findViewById<ImageView>(R.id.sr_account_type).setImageResource(acTypeRs)
        if(position != 0) view.setBackgroundResource(R.drawable.round_button_option_selector)
    }
}