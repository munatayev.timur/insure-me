package app.snappik.insureme.ui.dialog

import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractDialogFragment
import app.snappik.insureme.databinding.LayoutBillingBinding
import app.snappik.insureme.model.BillingProduct
import app.snappik.insureme.utils.analytics.IAnalytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BillingDialog(private val mProduct: BillingProduct,
                    private val onUpgradeClicked:() -> Unit) :
    AbstractDialogFragment<LayoutBillingBinding>(
        R.layout.layout_billing,
        R.style.FullScreenDialog
    ) {

    @Inject lateinit var analytics: IAnalytics

    override fun LayoutBillingBinding.initVariables() {
        dialog = this@BillingDialog
        product = mProduct
    }

    fun onUpgradeClicked(){
        analytics.tryToBuy()
        onUpgradeClicked.invoke()
    }
}