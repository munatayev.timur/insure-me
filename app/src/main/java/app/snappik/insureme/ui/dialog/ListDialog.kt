package app.snappik.insureme.ui.dialog

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import app.snappik.insureme.R
import app.snappik.insureme.common.AbstractDialogFragment
import app.snappik.insureme.databinding.LayoutListBinding
import app.snappik.insureme.ui.adapter.ListAdapter

class ListDialog(
    private val title: String,
    private val list: List<Int>,
    private val descriptions: List<Int>,
    private val disabled: List<Int>,
    private val selectedId: Int,
    private val onSelected: (res: Int) -> Unit
) : AbstractDialogFragment<LayoutListBinding>(R.layout.layout_list, R.style.SimpleDialog) {

    override fun LayoutListBinding.initVariables() {
        title = this@ListDialog.title
        dialog = this@ListDialog
        manager = LinearLayoutManager(context)
        adapter = ListAdapter(list, disabled, selectedId, descriptions, onSelected){ res, desc -> onShowPremium(res, desc) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
    }

    private val onPremiumUpdate = fun(){
        (viewDataBinding.adapter as? ListAdapter)?.disabled = emptyList()
        viewDataBinding.invalidateAll()
    }

    private fun onShowPremium(res: Int, desc: Int) = featureAvailableWithPremium(res, desc, title, onPremiumUpdate)
}