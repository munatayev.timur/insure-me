package app.snappik.insureme.ui.map

import android.os.Bundle
import app.snappik.insureme.common.AbstractActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapActivity : AbstractActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            changeFragment(MapFragment::class.java, intent.extras)
        }
    }

    override fun onBackPressed() = Unit
}