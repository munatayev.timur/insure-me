package app.snappik.insureme.ui.adapter

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageButton
import android.widget.TextView
import app.snappik.insureme.R
import app.snappik.insureme.model.entity.Friend

open class FriendsAdapter(
    open val isChooser: Boolean,
    open val onAction: (friend: Friend) -> Unit,
    open val onRename: ((friend: Friend) -> Unit)? = null,
    val onChoose: ((friend: Friend) -> Unit)? = null) : ICommonAdapter<Friend>() {

    fun contains(endpointId: String) = list.find { it.displayName == endpointId } != null

    override fun getLayoutId(): Int = R.layout.item_scanner_friend

    override fun getViewHolder(view: View): ViewHolder<Friend> = object : ViewHolder<Friend>(view) {
        override fun bind(item: Friend) {
            view.findViewById<ImageButton>(R.id.item_sc_rename).apply {
                visibility = if (item.id > 0) VISIBLE else GONE
            }.setOnClickListener { onRename?.invoke(item) }
            view.findViewById<ImageButton>(R.id.item_sc_remove).apply {
                setImageResource(if(item.id > 0) R.drawable.ic_remove else R.drawable.ic_add)
                setBackgroundResource(if(item.id > 0) R.drawable.round_red_square_button_selector else R.drawable.round_square_button_selector)
            }.setOnClickListener { onAction.invoke(item) }
            view.setOnClickListener { if (isChooser) onChoose?.invoke(item) }
            view.findViewById<TextView>(R.id.item_sc_name).text = item.displayName
        }
    }
}