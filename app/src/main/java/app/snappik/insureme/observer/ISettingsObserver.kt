package app.snappik.insureme.observer

import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.viewmodel.SettingsViewModel

interface ISettingsObserver : IAbstractObserver {

    val settingsViewModel: SettingsViewModel

    fun subscribeToSettingsUpdate() = settingsViewModel.settings?.subscribe {
        it?.let {
            onSettingsFetched(it)
        }
    }

    fun fetchSettings() = settingsViewModel.settings?.subscribe {
        it?.let {
            onSettingsFetched(it)
            settingsViewModel.settings?.unsubscribe()
        }
    }

    fun onSettingsFetched(settings: Settings)
}