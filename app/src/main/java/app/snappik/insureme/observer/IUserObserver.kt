package app.snappik.insureme.observer

import app.snappik.insureme.model.entity.User
import app.snappik.insureme.viewmodel.UserViewModel

interface IUserObserver : IAbstractObserver {

    val userViewModel: UserViewModel

    fun subscribeToUserUpdate() = userViewModel.user?.subscribe{
        it?.let { onUserFetched(it) } ?: noUser()
    }

    fun onUserFetched(user: User)

    fun noUser() = Unit
}