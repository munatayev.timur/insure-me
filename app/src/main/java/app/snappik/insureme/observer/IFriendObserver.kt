package app.snappik.insureme.observer

import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.viewmodel.FriendViewModel

interface IFriendObserver : IAbstractObserver {

    val friendViewModel : FriendViewModel

    fun subscribeToFriendsUpdate() = friendViewModel.friends?.subscribe{
        it?.let { onFriendsFetched(it) } ?: noFriends()
    }

    fun onFriendsFetched(list: List<Friend>)

    fun noFriends() = Unit
}