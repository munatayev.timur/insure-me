package app.snappik.insureme.observer

import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.viewmodel.ActionViewModel
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.NONE
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.PREPARE
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.PROCESSING
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.REQUIRED
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.TIME
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.FALL_DETECTED
import app.snappik.insureme.viewmodel.ActionViewModel.ActionStatus.SETTINGS

interface IActionObserver : IAbstractObserver {

    val actionViewModel: ActionViewModel

    fun subscribeToActionUpdate(settings: Settings) = actionViewModel.action.subscribe {
        it?.let {
            when (it) {
                NONE -> shutDownService()
                TIME -> startTimer(settings.getNonExactTime(), settings)
                PROCESSING -> doAction(settings)
                SETTINGS -> Unit
                PREPARE -> prepareAction()
                REQUIRED, FALL_DETECTED  -> {
                    startTimer(MINUTE, settings)
                    actionRequired()
                }
            }
        }
    }

    fun shutDownService()

    fun startTimer(time: Long, settings: Settings)

    fun actionRequired()

    fun doAction(settings: Settings)

    fun prepareAction()

    companion object {
        private const val MINUTE = 1000 * 60L
    }
}