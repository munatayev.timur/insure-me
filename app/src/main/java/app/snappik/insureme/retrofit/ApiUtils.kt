package app.snappik.insureme.retrofit

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiUtils {

    fun apiService(): ApiService = getApiService().create(ApiService::class.java)

    private fun getGson():Gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
        .create()

    private val clientBuilder = OkHttpClient.Builder().apply {
        dispatcher(Dispatcher().apply { maxRequests = 1 })
        readTimeout(10, TimeUnit.SECONDS)
    }.build()

    private fun getBaseUrl():String = "https://push-api.cloud.huawei.com/"

    private fun getApiService():Retrofit = Retrofit
        .Builder()
        .baseUrl(getBaseUrl())
        .addConverterFactory(GsonConverterFactory.create(getGson()))
        .client(clientBuilder)
        .build()
}