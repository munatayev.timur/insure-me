package app.snappik.insureme.retrofit

import androidx.annotation.WorkerThread
import app.snappik.insureme.model.request.GooglePushRequest
import app.snappik.insureme.model.request.HuaweiPushRequest
import javax.inject.Inject

class RetrofitRepository @Inject constructor(private val apiService: ApiService) {

    @WorkerThread suspend fun sendPush(appId: String, accessToken: String, huaweiPushRequest: HuaweiPushRequest){
        try{
            apiService.sendHuaweiPush("$appId/messages:send", accessToken, huaweiPushRequest)
        }catch (exp: Exception){}
    }

    @WorkerThread suspend fun sendGooglePush(projectId: String, token: String, request: GooglePushRequest){
        try{
            apiService.sendGooglePush("https://fcm.googleapis.com/v1/projects/${projectId}/messages:send", "Bearer $token", request)
        }catch (exp: Exception){}
    }
}