package app.snappik.insureme.retrofit

import app.snappik.insureme.model.request.GooglePushRequest
import app.snappik.insureme.model.request.HuaweiPushRequest
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Url

interface ApiService {
    @POST suspend fun sendHuaweiPush(@Url url:String, @Header("Authorization") authToken:String, @Body huaweiPushRequest: HuaweiPushRequest)
    @POST suspend fun sendGooglePush(@Url url:String, @Header("Authorization") authToken:String, @Body push: GooglePushRequest)
}