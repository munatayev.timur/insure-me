package app.snappik.insureme.database.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import app.snappik.insureme.database.dao.SettingsDao
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.utils.analytics.IAnalytics
import javax.inject.Inject

class SettingsRepository@Inject constructor(private val settingsDao: SettingsDao,
                                            private val analytics: IAnalytics) {

    @WorkerThread fun getSettings(): LiveData<Settings> = settingsDao.getSettings()
    @WorkerThread suspend fun updateSettings(settings: Settings) {
        analytics.settingsUpdated(settings)
        settingsDao.update(settings)
    }
}