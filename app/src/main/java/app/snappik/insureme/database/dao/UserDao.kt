package app.snappik.insureme.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.snappik.insureme.model.entity.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user ORDER BY id DESC LIMIT 1")
    fun getUser(): LiveData<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User)

    @Delete
    suspend fun delete(user: User)

    @Query("UPDATE user SET isAgreedWithTerms=1 WHERE id=:userId")
    suspend fun applyTermsAndConditions(userId: Long)
}