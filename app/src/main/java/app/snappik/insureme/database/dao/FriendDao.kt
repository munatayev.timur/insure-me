package app.snappik.insureme.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import app.snappik.insureme.model.entity.Friend

@Dao
interface FriendDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(friend: Friend)

    @Query("DELETE from friend")
    suspend fun deleteAll()

    @Query("SELECT * from friend")
    fun getAll(): LiveData<List<Friend>>

    @Delete
    suspend fun delete(friend: Friend)

    @Query("DELETE from friend WHERE token=:mToken")
    suspend fun deleteByToken(mToken: String)

    @Update
    suspend fun update(friend: Friend)
}