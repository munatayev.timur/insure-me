package app.snappik.insureme.database.migrations

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

class MigrationTo2 : Migration(1,2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE Settings ADD COLUMN fallDetector INTEGER NOT NULL DEFAULT 0")
    }
}