package app.snappik.insureme.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import app.snappik.insureme.database.converter.AlarmTypeConverter
import app.snappik.insureme.database.converter.StrategyListConverter
import app.snappik.insureme.database.converter.UnlockTypeConverter
import app.snappik.insureme.database.converter.UserTypeConverter
import app.snappik.insureme.database.dao.FriendDao
import app.snappik.insureme.database.dao.SettingsDao
import app.snappik.insureme.database.dao.UserDao
import app.snappik.insureme.database.migrations.MigrationTo2
import app.snappik.insureme.model.entity.Friend
import app.snappik.insureme.model.entity.Settings
import app.snappik.insureme.model.entity.User
import java.util.concurrent.Executors

@Database(entities = [User::class, Friend::class, Settings::class], version = 2, exportSchema = false)
@TypeConverters(value = [
    StrategyListConverter::class,
    UnlockTypeConverter::class,
    AlarmTypeConverter::class,
    UserTypeConverter::class])
abstract class LocalDatabase : RoomDatabase() {
    abstract val userDao: UserDao
    abstract val friendDao: FriendDao
    abstract val settingsDao: SettingsDao

    companion object{

        private const val DATABASE_NAME = "insureme.db"

        @Volatile private var instance: LocalDatabase? = null

        fun getDatabase(context: Context): LocalDatabase =
            instance ?: synchronized(this) { instance ?: buildDatabase(context).also { instance = it } }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, LocalDatabase::class.java, DATABASE_NAME)
                .addMigrations(MigrationTo2())
                .addCallback(object : RoomDatabase.Callback(){
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        Executors.newSingleThreadExecutor().execute {
                            instance?.settingsDao?.insert(Settings())
                        }
                    }
                })
                .build()
    }
}