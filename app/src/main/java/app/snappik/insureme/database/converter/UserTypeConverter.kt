package app.snappik.insureme.database.converter

import androidx.room.TypeConverter
import app.snappik.insureme.model.entity.User

class UserTypeConverter {
    @TypeConverter
    fun fromListToString(type: User.Type): String = type.toString()

    @TypeConverter
    fun fromStringToList(line: String): User.Type = line.run { User.Type.valueOf(line) }
}