package app.snappik.insureme.database.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import app.snappik.insureme.database.dao.UserDao
import app.snappik.insureme.model.entity.User
import javax.inject.Inject

class UserRepository @Inject constructor(private val userDao: UserDao) {
    @WorkerThread suspend fun signInUser(user : User) = userDao.insert(user)
    @WorkerThread suspend fun deleteUser(user: User) = userDao.delete(user)
    @WorkerThread fun getUser(): LiveData<User> = userDao.getUser()
    @WorkerThread suspend fun applyTermsAndConditions(userId: Long) = userDao.applyTermsAndConditions(userId)
}