package app.snappik.insureme.database.converter

import androidx.room.TypeConverter
import app.snappik.insureme.model.Strategy

class StrategyListConverter {
    @TypeConverter
    fun fromListToString(list: HashSet<Strategy>): String {
        return if (list.isEmpty()) ""
        else list.map { it.res }
            .joinToString(separator = ",")
    }

    @TypeConverter
    fun fromStringToList(line: String): HashSet<Strategy> {
        return if (line.isEmpty()) hashSetOf() else line.split(',')
            .map { it.toInt() }
            .map { Strategy.findByRes(it) }
            .toHashSet()
    }
}