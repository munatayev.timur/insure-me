package app.snappik.insureme.database.converter

import androidx.room.TypeConverter
import app.snappik.insureme.model.AlarmType

class AlarmTypeConverter {
    @TypeConverter
    fun fromListToString(type: AlarmType): String = type.res.toString()

    @TypeConverter
    fun fromStringToList(line: String): AlarmType = line.toInt().run { AlarmType.findByRes(this) }
}