package app.snappik.insureme.database.converter

import androidx.room.TypeConverter
import app.snappik.insureme.model.UnlockType

class UnlockTypeConverter {
    @TypeConverter
    fun fromListToString(type: UnlockType): String = type.res.toString()

    @TypeConverter
    fun fromStringToList(line: String): UnlockType = line.toInt().run { UnlockType.findByRes(this) }
}