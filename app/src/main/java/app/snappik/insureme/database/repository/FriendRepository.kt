package app.snappik.insureme.database.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import app.snappik.insureme.database.dao.FriendDao
import app.snappik.insureme.model.entity.Friend
import javax.inject.Inject

class FriendRepository @Inject constructor(private val friendDao: FriendDao) {
    @WorkerThread suspend fun deleteAll() = friendDao.deleteAll()
    @WorkerThread suspend fun deleteFriend(friend: Friend) = friendDao.delete(friend)
    @WorkerThread suspend fun deleteFriendByToken(token: String) = friendDao.deleteByToken(token)
    @WorkerThread fun insert(friend: Friend) = friendDao.insert(friend)
    @WorkerThread suspend fun update(friend: Friend) = friendDao.update(friend)
    @WorkerThread fun getAll(): LiveData<List<Friend>> = friendDao.getAll()
}