package app.snappik.insureme.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.annotation.CallSuper

//important for working hilt in children
abstract class AbstractReceiver : BroadcastReceiver() {
    @CallSuper override fun onReceive(context: Context, incomeIntent: Intent) = Unit
}