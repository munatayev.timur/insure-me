package app.snappik.insureme.receiver

import android.content.Context
import android.content.Intent
import app.snappik.insureme.BuildConfig
import app.snappik.insureme.utils.wakelock.WakeLockManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AlarmReceiver : AbstractReceiver() {

    @Inject lateinit var wakeLockManager: WakeLockManager

    override fun onReceive(context: Context, incomeIntent: Intent) {
        super.onReceive(context, incomeIntent)
        val outcomeIntent = Intent(ACTION_RING)
       when(incomeIntent.action){
            ACTION_FIRE -> {
                wakeLockManager.acquireTransitionWakeLock(outcomeIntent)
                context.sendBroadcast(outcomeIntent)
            }
       }
    }

    companion object{
        const val ACTION_FIRE = BuildConfig.APPLICATION_ID + ".ACTION_FIRE"
        const val ACTION_RING = BuildConfig.APPLICATION_ID + ".ACTION_RING"
    }
}