package app.snappik.insureme.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import app.snappik.insureme.BuildConfig
import app.snappik.insureme.service.FallDetectorService
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, incomeIntent: Intent) {
        if(incomeIntent.action == BOOT_COMPLETED
            || incomeIntent.action == BOOT_COMPLETED_LOCKED
            || incomeIntent.action == ACTION_FALL_DETECTOR){
            FallDetectorService.checkAndStart(context)
        }
    }

    companion object{
        const val ACTION_FALL_DETECTOR = BuildConfig.APPLICATION_ID + ".ACTION_FALL_DETECTOR"
        private const val BOOT_COMPLETED = Intent.ACTION_BOOT_COMPLETED
        private const val BOOT_COMPLETED_LOCKED = Intent.ACTION_LOCKED_BOOT_COMPLETED
    }
}