package app.snappik.insureme.common

import android.os.Build
import android.os.Bundle
import android.view.WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
import android.view.WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
import android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
import android.view.WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
import android.view.WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
import androidx.appcompat.app.AppCompatActivity
import app.snappik.insureme.R

abstract class AbstractActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_abstract_fullscreen)
        showWhenLocked()
        disableAnimationBetweenActivities()
    }

    private fun disableAnimationBetweenActivities() = with(window){
        allowEnterTransitionOverlap = false
        allowReturnTransitionOverlap = false
        exitTransition = null
        enterTransition = null
        overridePendingTransition(0, 0)
    }

    private fun showWhenLocked(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
        } else {
            window.addFlags(
                FLAG_SHOW_WHEN_LOCKED or FLAG_DISMISS_KEYGUARD or FLAG_KEEP_SCREEN_ON
                    or FLAG_TURN_SCREEN_ON  or FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
            )
        }
    }

    fun changeFragment(fragmentClass: Class<out AbstractFragment<*>>, bundle: Bundle? = null) {
        supportFragmentManager.fragmentFactory.instantiate(classLoader, fragmentClass.name).also {
            supportFragmentManager.beginTransaction().replace(R.id.container, fragmentClass, bundle, fragmentClass.name).commit()
        }
    }

    inline fun <reified T : AbstractFragment<*>> getFragment(): T? = supportFragmentManager.findFragmentByTag(T::class.java.name) as? T
}