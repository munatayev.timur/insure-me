package app.snappik.insureme.common

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class AbstractLayoutDataBinding<T : ViewDataBinding> : FrameLayout, IDataBinding<T> {

    var fragment: AbstractFragment<*>? = null

    constructor(context: Context) : super(context) {
        initView()
    }

    constructor(context: Context, attr: AttributeSet) : super(context, attr) {
        initView()
    }

    constructor(context: Context, attr: AttributeSet, i: Int) : super(context, attr, i) {
        initView()
    }

    override lateinit var viewDataBinding: T

    protected val invalidateUnit = fun() { viewDataBinding.invalidateAll() }

    abstract fun getViewId(): Int

    abstract fun T.initVariables()

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        viewDataBinding.initVariables()
        viewDataBinding.invalidateAll()
    }

    private fun initView() {
        viewDataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            getViewId(),
            this,
            true
        )
    }
}