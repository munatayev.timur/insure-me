package app.snappik.insureme.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import app.snappik.insureme.R
import app.snappik.insureme.ui.dialog.InfoDialog
import app.snappik.insureme.ui.main.MainActivity
import app.snappik.insureme.utils.permissions.IPermissions

abstract class AbstractDialogFragment<T : ViewDataBinding>(
    private val viewId: Int,
    private val styleId: Int
) :
    DialogFragment(),
    IPermissions.Callback,
    IDataBinding<T>{

    override lateinit var viewDataBinding: T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, viewId, container, false)
        viewDataBinding.initVariables()
        return viewDataBinding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, styleId)
    }

    abstract fun T.initVariables()

    open fun show(manager: FragmentManager) {
        if (manager.isStateSaved.not()) {
            super.show(manager, tag ?: this::class.java.name)
        }
    }

    fun featureAvailableWithPremium(res: Int, description: Int, type: String?, onBought: () -> Unit) {
        val title = "$type: ${getString(res)}"
        val message = "${getString(description)}${getString(R.string.billing_do_you_want_to_show)}"
        InfoDialog(title, message, onOk = {
            (activity as? MainActivity)?.showBillingDialog(onBought)
        }, onCancel = {}).show(childFragmentManager)
    }

    fun featureNotAvailableAsGuest() = InfoDialog(getString(R.string.general_not_available),
                getString(R.string.login_not_available_for_guest)).show(childFragmentManager)

}