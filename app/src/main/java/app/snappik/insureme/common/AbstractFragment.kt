package app.snappik.insureme.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import app.snappik.insureme.R
import app.snappik.insureme.ui.dialog.InfoDialog
import app.snappik.insureme.ui.main.MainActivity
import app.snappik.insureme.utils.permissions.IPermissions

abstract class AbstractFragment<T : ViewDataBinding> : Fragment(), IPermissions.Callback {

    abstract val viewId: Int

    lateinit var viewDataBinding: T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(layoutInflater, viewId, container, false)
        viewDataBinding.initVariables()
        viewDataBinding.lifecycleOwner = this
        return viewDataBinding.root
    }

    abstract fun T.initVariables()

    open fun onBackPressed() = Unit

    protected inline fun <reified T> getParentActivity(): T? = if(activity is T) (activity as T) else null

    infix fun Int.replace(fragment: Fragment){
        parentFragmentManager.beginTransaction().run {
            replace(this@replace, fragment)
            commit()
        }
    }

    fun closeFragment(){
        parentFragmentManager.fragments.lastOrNull()?.let {
            parentFragmentManager.beginTransaction().run {
                remove(it)
                commit()
            }
        }
    }

    fun featureAvailableWithPremium(res: Int, description: Int) {
        val title = getString(res)
        val message = "${getString(description)}${getString(R.string.billing_do_you_want_to_show)}"
        InfoDialog(title, message, onOk = {
            (activity as? MainActivity)?.showBillingDialog { viewDataBinding.invalidateAll() }
        }, onCancel = {}).show(parentFragmentManager)
    }

    fun getStringByRes(res: Int) = if(res != 0)  super.getString(res) else null
}